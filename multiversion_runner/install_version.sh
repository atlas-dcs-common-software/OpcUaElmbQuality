#!/bin/sh

VERSION=$1
if [ -d OpcUaElmbQuality_versions/OpcUaElmbQuality-$VERSION ]; then
	echo "Sorry but seems that the version is already installed!"
	return -1
fi

/usr/bin/svn export https://svn.cern.ch/reps/atlasdcs/OpcUaElmbQuality/tags/OpcUaElmbQuality-$VERSION/ OpcUaElmbQuality_versions/OpcUaElmbQuality-$VERSION
# TODO checking errors after svn


