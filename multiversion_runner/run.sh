#!/bin/sh

if [ "$#" -lt 2 ] ; then
	echo 'Minimum 2 args needed <suite version> <options to run_all.sh e.g. --run_sock or --run_simulated>'
	exit -1
fi

SUITE_VERSION=$1

echo "Checking for version of the installed server"
SERVER_VERSION=`rpm -q --qf "%{VERSION}-%{RELEASE}" OpcUaCanOpenServer`

echo "Found version $SERVER_VERSION"

if [ ! -d results/$SERVER_VERSION ]; then
	echo "Results directory for version $SERVER_VERSION not yet existing -- creating. "
	mkdir results/$SERVER_VERSION
fi



OUTPUT_DIR=results/$SERVER_VERSION/$SUITE_VERSION

if [ -d $OUTPUT_DIR ]; then
	echo "Results dir $OUTPUT_DIR already exists; exiting!"
	exit -1
fi

mkdir $OUTPUT_DIR

SUITE_DIR="OpcUaElmbQuality_versions/OpcUaElmbQuality-$SUITE_VERSION"
sh -c "cd $SUITE_DIR; python run_all.py --run_simulated --run_sock --limit_detailed_reports "

echo "Tests finished... copying results to $OUTPUT_DIR"
/bin/cp $SUITE_DIR/output/* $OUTPUT_DIR

echo "Regenerating index ..."

python create_index.py
