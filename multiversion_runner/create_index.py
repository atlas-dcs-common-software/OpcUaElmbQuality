#!/usr/bin/python

import markup
import os
import os.path
import socket

def findListSuites():
	suites=[]
	dirsInResults = os.listdir('results/')
	for dir in dirsInResults:
		if not os.path.isdir('results/'+dir):
			continue
		if dir[0] == '.':
			continue
		suitesInResult=os.listdir('results/'+dir)
		for s in suitesInResult:
			if not s in suites:
				suites.append(s)
	return suites
def findListServers():
	servers=[]
	entries= os.listdir('results/')
	for e in entries:
		if e[0]=='.':
			continue
		if os.path.isdir('results/'+e):
			servers.append(e)
	return servers


suites=findListSuites()
suites.sort()
servers=findListServers()
servers.sort()
page=markup.page()
page.init(title="Browsing results from OpcUaElmbQuality")
page.p('Please find results from various version of the OpcUaCanOpenServer and the OpcUaElmbQuality below. All results are from '+socket.gethostname())

page.table(border="1")
page.tr()
page.td('Server \ Suite')
page.td.close()
for s in suites:
	page.td(s)
	page.td.close()
page.tr.close()
for server in servers:
	page.tr()
	page.td(server)
	page.td.close()
	for s in suites:
		page.td()
		summary_html_path=server+'/'+s+'/summary.html'
		if os.path.isfile('results/'+summary_html_path):
			page.a('Click', href=summary_html_path)
		page.td.close()
		

f=open('results/index.html','w')
f.write(str(page))
f.close()

