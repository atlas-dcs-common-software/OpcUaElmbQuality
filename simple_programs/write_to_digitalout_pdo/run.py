#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
import dcs_server_config
import dcs_sim_config
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives import *
from optparse 		    import OptionParser
import	random



try:
	dcs_opc_ua_elmb.connect()	
	addr = Address(NodeId('can0.elmb1.do_write',dcs_opc_ua_elmb.server_ns), dcs_opc_ua_elmb.server_uri)
	res=dcs_opc_ua_elmb.client.write ([addr], [Byte(200)] )
	print 'Result='+str(res.targets[0].status)
except Exception as e:
	print 'Exception: '+str(e)


