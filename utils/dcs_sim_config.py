import os
template_path='../../utils/'

name_prologue = 'sim_config_prologue.xml'
name_epilogue = 'sim_config_epilogue.xml'


def write_prologue(filename):
	if os.system('cp '+template_path+name_prologue+' '+filename) != 0:
		raise

def write_epilogue(filename):
	if os.system('cat '+template_path+name_epilogue+' >> '+filename) != 0:
		raise


def create_basic_sim_config(buses,nodes,busAttrs=None,nodeAttrs=None,logFrames=False):
	"""Create a VirtualElmbSimulator config file composed of number of identical buses that differ only by their bus id.
	
	nodes may be given as a integer or a list 
	"""
	filename='sim_config.xml'
	write_prologue(filename)
	f=open(filename,'a')
	if busAttrs==None:
		busAttrs=''
	if nodeAttrs==None:
		nodeAttrs=''
	if type(nodes)==int:
		nodes=range(1,nodes+1)
	for bus in range(0,buses):
		bus_name='can'+str(bus)
		f.write ('<bus portName="'+bus_name+'" '+busAttrs)
		if logFrames:
			f.write('framesDumpFileName="can'+str(bus)+'_frames.txt" ')
		f.write (' >\n');
		for node in nodes:
			f.write('<node type="elmb" id="'+str(node)+'" '+nodeAttrs+' />\n')
		f.write('</bus>\n')
	f.close()	
	write_epilogue(filename)
	
