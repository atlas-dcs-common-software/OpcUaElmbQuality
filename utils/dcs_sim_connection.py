import socket
import time

from pyuaf.util.primitives  import Byte 

connection_socket=[]

server_address='127.0.0.1'
server_port=9911
response_size=64

def open_connection():
	global connection_socket
	connection_socket = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
	print("Trying to connect to simulator TextCommands using TCP/IP...")
	num_attemps=0
	while (True):
		try:
			connection_socket.connect((server_address, server_port))
		except Exception as e:
			num_attemps = num_attemps+1
			if num_attemps < 20:
				print('Will try once again')
				time.sleep(3)
				continue
			else:
				raise "Couldnt connect to simulator."
		print ("Connected")
		return

		
	
def get_node_nmt_state(bus,node):
	connection_socket.send ('getnodenmtstate '+bus+' '+str(node)+' ');
	connection_socket.settimeout (1.0)
#	time.sleep (0.2)
	return (str(connection_socket.recv(response_size))).strip()

def set_analog_input(bus,node,channel,value):
	connection_socket.send ('setanaloginput '+bus+' '+str(node)+' '+str(channel)+' '+str(value));
	time.sleep (1)
	
def get_num_received_rtr_tpdo3(bus,node):
	connection_socket.send ('get_num_received_rtr_tpdo3 '+bus+' '+str(node)+' ');
	connection_socket.settimeout (1.0)
	return int(connection_socket.recv(response_size))

def get_num_received_sync(bus,node):
	connection_socket.send ('get_num_received_sync '+bus+' '+str(node)+' ');
	connection_socket.settimeout (1.0)
	return int(connection_socket.recv(response_size))

def get_num_received_nodeguard(bus,node):
	connection_socket.send ('get_num_received_nodeguard '+bus+' '+str(node)+' ');
	connection_socket.settimeout (1.0)
	return int(connection_socket.recv(response_size))

def get_array_register(bus,node,register,byte_from,byte_num):
	connection_socket.send ('getarrayregister '+bus+' '+str(node)+' '+register+' '+str(byte_from)+' '+str(byte_num))
	connection_socket.settimeout (1.0)
	resp = connection_socket.recv(byte_num*2 + 64)
#	print 'TCP/IP response='+resp
	# check for sane length
	if (len(resp) < byte_num*2):
		raise Exception ('received length from the simulator is too short')
	# if len is at least 4 we can verify for error
	if (len(resp) >= 4):
		if resp[0:4] == 'FAIL':
			raise Exception (str(resp))
		# might be that we should also look for error
	# now parse such that we get array of bytes
	# we hope that length is correct
	out=[]
	for i in range(0,byte_num):
		ss = resp[2*i:2*i+2] # cut 2 hex characters representing result
	#	print 'now processing:'+ss
		ss_int = int(ss,16)  # hex parsing
		b = Byte(ss_int) 
		out.append(b)
	return out 
	
def compare_by_chunks(bus,node,register,test_bytes):
	errors=0
	max_chunk_size=16
	curr_pos=0
	to_go=len(test_bytes)
	while to_go>0:
		this_time=to_go
		if this_time>max_chunk_size:
			this_time=max_chunk_size
		result=get_array_register(bus,node,register,curr_pos,this_time)
		for i in range(0,this_time):
			if test_bytes[curr_pos+i].value != result[i].value:
				errors = errors+1
		to_go = to_go-this_time
		curr_pos += this_time
	return errors	

def write_segmented_sdo_to_file(bus,node):
	connection_socket.send ('write_segmented_sdo_to_file '+bus+' '+str(node)+' ');

def all_write_segmented_sdo_to_file():
	connection_socket.send ('all_write_segmented_sdo_to_file ');
	# wait some time for 'DONE'
	print('Sent all_write_segmented_sdo_to_file, now waiting for DONE ...')
	connection_socket.settimeout (30.0)
	s = connection_socket.recv(response_size)
	if s=='DONE':
		print ('DONE received ==> ok')
	else:
		print ('Nothing received or some shit:"'+str(s)+'" ==> BAD')
	

def dump_frames_log():
	connection_socket.send ('dump_frames_log ');

def get_test_buffer_9999(bus,node):
	connection_socket.send ('read_test9999 '+bus+' '+str(node))
	connection_socket.settimeout (1.0)
	return connection_socket.recv(response_size)


def set_test_buffer_9999(bus,node,buff):
	connection_socket.send ('write_test9999 '+bus+' '+str(node)+' '+buff)
	connection_socket.settimeout (1.0)
	return connection_socket.recv(response_size)

def set_conversion_flag (bus,node,ch,buff):
	connection_socket.send ('set_conversion_flag '+bus+' '+str(node)+' '+str(ch)+' '+buff)
	connection_socket.settimeout (1.0)
	return connection_socket.recv(response_size)
