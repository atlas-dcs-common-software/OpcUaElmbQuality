import dcs_test_utils
import dcs_opc_ua_elmb 
import dcs_sim_connection

# This library build on top of OPC UA connectivity provided  by dcs_opc_ua_elmb, but adds automatic reporting of errors for through dcs_test_utils for autotest purposes

def call_method_bus (bus,method):
	method_addr=dcs_opc_ua_elmb.bus_obj_addr(bus,method)
	target_addr=dcs_opc_ua_elmb.bus_addr(bus)
	try:
		if str(dcs_opc_ua_elmb.client.call(target_addr, method_addr, [], None, None).overallStatus)=='Good':
			dcs_test_utils.log_detail(True,method+'()',bus)
		else:
			dcs_test_utils.log_detail(False,method+'()',bus)
	except Exception as e:
		dcs_test_utils.log_detail(False,method+'(): EXCEPTION '+str(e),bus)

def call_method_node (bus,node,method):
	method_addr=dcs_opc_ua_elmb.node_obj_addr(bus,node,method)
	target_addr=dcs_opc_ua_elmb.node_addr(bus,node)
	try:
		if str(dcs_opc_ua_elmb.client.call(target_addr, method_addr, [], None, None).overallStatus)=='Good':
			dcs_test_utils.log_detail(True,method+'()',bus+' '+str(node))
		else:
			dcs_test_utils.log_detail(False,method+'()',bus+' '+str(node))
	except Exception as e:
		dcs_test_utils.log_detail(False,method+'(): EXCEPTION '+str(e),bus+' '+str(node))

def read_expect_good (addr):
	try:
		result=dcs_opc_ua_elmb.client.read([addr])
		if str(result.overallStatus)!='Good':
			dcs_test_utils.log_detail(False,'(opcua)Read: status not good ', dcs_opc_ua_elmb.addr_to_str(addr));
			return (False,None)
		else:
			return (True,result)
		
	except Exception as e:
		dcs_test_utils.log_detail(False,'(opcua)Read EXCEPTION '+str(e), dcs_opc_ua_elmb.addr_to_str(addr));
		return (False,None)

def sim_set_analog_input(bus,node,channel,val):
	try:
		dcs_sim_connection.set_analog_input(bus,node,channel,val)
	except Exception as e:
		dcs_test_utils.log_detail(False,'sim_set_analog_input Exception:'+str(e),str(bus)+' '+str(node))

