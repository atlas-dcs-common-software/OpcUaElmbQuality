import time
import pyuaf
from pyuaf.util             import Address, NodeId, opcuaidentifiers
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings, SessionSettings
from pyuaf.util             import loglevels
import dcs_test_utils

#Constants (??)

server_address='opc.tcp://localhost:48010'
server_uri='urn:AtlasDcs:OpcUaCanOpenServer'
server_ns=2

client = []
def connect():
	global client
	cs = ClientSettings("myClient", [server_address])
# Uncomment below for session logging
#	cs.logToStdOutLevel = loglevels.Info 
	max_attempts=20
	num_attempts=0
	while (True):
		result=None
		try:
			print 'Trying to connect to OPC UA'
			client = Client(cs)
			rootNode = Address( NodeId(opcuaidentifiers.OpcUaId_RootFolder, 0), server_uri )
			result=client.browse ([ rootNode ])
		except Exception as e:
			num_attempts = num_attempts+1
			if num_attempts > max_attempts:
				msg = 'Despite '+str(max_attempts)+ ' attempts couldnt establish OPC UA connection: exception is '+str(e)
				print msg
				dcs_test_utils.log_detail(False,msg,'')
				raise msg
			time.sleep(3)
			continue
		print 'Connected to OPC UA Server'
		return
			
		

def addr(sa):
	return Address(NodeId(sa,server_ns), server_uri)

def bus_obj_addr(bus,obj):
	return Address(NodeId(bus+'.'+obj,server_ns), server_uri)
def bus_addr(bus):
	return Address(NodeId(bus,server_ns), server_uri)

def node_addr(bus,node):
	return Address(NodeId(bus+'.elmb'+str(node),server_ns), server_uri)
def node_obj_addr(bus,node,obj):
	return Address(NodeId(bus+'.elmb'+str(node)+'.'+obj,server_ns), server_uri)

def bus_sync(bus):
	return str(client.call(bus_addr(bus), bus_obj_addr(bus,'Synch') ,[],None,None).overallStatus)
def bus_start(bus):
	return str(client.call(bus_addr(bus), bus_obj_addr(bus,'Start') ,[],None,None).overallStatus)
def bus_stop(bus):
	return str(client.call(bus_addr(bus), bus_obj_addr(bus,'Stop') ,[],None,None).overallStatus)

def bus_reset(bus):
	return str(client.call(bus_addr(bus), bus_obj_addr(bus,'Reset')   ,[],None,None).overallStatus)

def bus_preoperation(bus):
	return str(client.call(bus_addr(bus), bus_obj_addr(bus,'PreOperation')  ,[],None,None).overallStatus)

def tpdo3_rtr(bus,node):
	addr_tpdo3_cmd = node_obj_addr(bus,node,'TPDO3.RTR')
	addr_tpdo3 = node_obj_addr(bus,node,'TPDO3')
	return client.call(addr_tpdo3, addr_tpdo3_cmd ,[],None,None)

def node_nmt_state(bus,node):
	result = client.read(node_obj_addr(bus,node,'State'))
	if (str(result.overallStatus) != 'Good'): 
		return '? (cant read)'
	else:
		state_int = int(result.targets[0].data.value) & 127
		if (state_int == 0):
			return 'initialising'
		elif (state_int == 4):
			return 'stopped'
		elif (state_int == 5):
			return 'operational'
		elif (state_int == 127):
			return 'preoperational'
		else:
			return '? unknown state '+str(state_int)
		

 

def pdo3_addr(can,elmb,channel):
	someAddress = Address(NodeId(str(can)+'.elmb'+str(elmb)+'.TPDO3.Value_'+str(channel), server_ns), server_uri)
	return someAddress
def addr_to_str(address):
	return address.getExpandedNodeId().nodeId().identifier().idString

