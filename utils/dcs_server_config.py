import os
import dcs_test_utils
template_path='../../utils/'

name_prologue = 'OPCUACANOpenServer_prologue.xml'
name_epilogue = 'OPCUACANOpenServer_epilogue.xml'

bitrate=125000
connection_type='simulated'

def write_prologue(filename,type):
	my_name_prologue = name_prologue
	if (type != None):
		my_name_prologue = 'OPCUACANOpenServer_prologue_'+type+'.xml'
	if os.system('cp '+template_path+my_name_prologue+' '+filename) != 0:
		raise

def write_epilogue(filename):
	if os.system('cat '+template_path+name_epilogue+' >> '+filename) != 0:
		raise

def create_basic_server_config(buses,nodes,nodeguard,sync,type=None, bus_attributes='', node_attributes='' ):
	filename='OPCUACANOpenServer.xml.source'
	write_prologue(filename,type)
	elmb_type='ELMB'
	if type!=None:
		if type=='mdt':
			elmb_type='MDM_BARREL'
	f=open(filename,'a')
	
	for bus in range(0,buses):
		bus_name='can'+str(bus)
		f.write ('<CANBUS speed="'+str(bitrate)+'" port="'+bus_name+'" type="'+connection_type+'" name="'+bus_name+'" '+bus_attributes+' >\n');
		f.write ('<NODEGUARD interval="'+str(nodeguard)+'"/>\n')
		f.write ('<SYNC interval="'+str(sync)+'"/>\n')
		for node in range(1,nodes+1):
			f.write('<NODE type="'+elmb_type+'" name="elmb'+str(node)+'" nodeid="'+str(node)+'" '+node_attributes+' />\n')
		f.write('</CANBUS>\n')
	f.close()	
	write_epilogue(filename)
	
