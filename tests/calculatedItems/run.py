#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_server_config
import dcs_conn_log
import dcs_sim_connection
import math
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
import pdb
accuracy=0.000000001

def verify_items_values():
	addr=dcs_opc_ua_elmb.node_obj_addr('can0',1,'trig_identity')
	addr_s=dcs_opc_ua_elmb.addr_to_str(addr)
	(success,result) = dcs_conn_log.read_expect_good(addr)
	trig_identity=result.targets[0].data.value
	if trig_identity==1.0:
		dcs_test_utils.log_detail(True,'trig_identity='+str(trig_identity),addr_s)
	else:
		dcs_test_utils.log_detail(False,'trig_identity='+str(trig_identity),addr_s)

def compare_float_accuracy(a,b):
	if a==0.0:
		return b==0.0
	if math.fabs((a-b)/a) < accuracy:
		return True
	else:
		return False

def verify_item_value(item,expected,sync_time):
	addr=dcs_opc_ua_elmb.node_obj_addr('can0',1,item)
	addr_s=dcs_opc_ua_elmb.addr_to_str(addr)
	(success,result) = dcs_conn_log.read_expect_good(addr)
	print(str(success))
	if success:
		print(str(success)+" "+str(result.targets[0].data))
		if compare_float_accuracy(result.targets[0].data.value,expected):
			dcs_test_utils.log_detail(True, 'OK val='+str(expected), addr_s)
		else:
			dcs_test_utils.log_detail(False, 'Bad value='+str(result.targets[0].data.value)+' expected'+str(expected), addr_s)
		time_diff = result.targets[0].serverTimestamp.toTime_t() - sync_time
		if time_diff>=0 and time_diff<15:
			dcs_test_utils.log_detail(True,'Server timestamp fine,time_diff='+str(time_diff),addr_s)
		else:
			dcs_test_utils.log_detail(False,'Server timestamp fail,time_diff='+str(time_diff),addr_s)
		time_diff = result.targets[0].sourceTimestamp.toTime_t() - sync_time
		if time_diff>=0 and time_diff<15:
			dcs_test_utils.log_detail(True,'Source timestamp fine,time_diff='+str(time_diff),addr_s)
		else:
			dcs_test_utils.log_detail(False,'Source timestamp fail,time_diff='+str(time_diff),addr_s)

subscription_came_flag=False
def subscription_verification(d):
	global subscription_came_flag
	subscription_came_flag=True


time.sleep(10)
dcs_test_utils.parse_options()
dcs_test_utils.start_server()
time.sleep(15)
dcs_sim_connection.open_connection()
try:
	dcs_opc_ua_elmb.connect()	
	dcs_conn_log.call_method_bus('can0','Start')
	time.sleep(5)
	for it in range(0,5):
		sin_1_input=it-4
		log_2_input=it+1 #avoid negatives obviously
		dcs_conn_log.sim_set_analog_input('can0',1,1,sin_1_input)
		dcs_conn_log.sim_set_analog_input('can0',1,2,log_2_input)
		dcs_conn_log.call_method_bus('can0','Synch')
		sync_time=pyuaf.util.DateTime.now().toTime_t()
		time.sleep(2)
		#verify_items_values()
		verify_item_value('TPDO3.Value_1',sin_1_input, sync_time)
		verify_item_value('TPDO3.Value_2',log_2_input, sync_time)
		print("sin "+str(sin_1_input))
		verify_item_value('sin_1',math.sin(sin_1_input),sync_time)
		print("log "+str(log_2_input))
		verify_item_value('log_2',math.log(log_2_input),sync_time)
	# verify that 'bad' is signalised on numerically incorrect expression like logarithm of negative
	dcs_test_utils.log_detail(None,'Now will test incorrect math expressions','')
	dcs_conn_log.sim_set_analog_input('can0',1,2,-1)
	dcs_conn_log.call_method_bus('can0','Synch')
	time.sleep(1)
	addr=dcs_opc_ua_elmb.node_obj_addr('can0',1,'log_2')
	addr_s=dcs_opc_ua_elmb.addr_to_str(addr)
	try:
		result=dcs_opc_ua_elmb.client.read([addr])
                #pdb.set_trace()
		if not result.targets[0].status.isBad():
			dcs_test_utils.log_detail(False,'Expected Bad status, but got:'+str(result.overallStatus),addr_s)
		else:
			dcs_test_utils.log_detail(True,'OK - log(-1) returned Bad status',addr_s)
	except:
		dcs_test_utils.log_detail(False,'Exception when read()',addr_s)

	# create a subscription on a calculated item and verify if it is properly executed
	dcs_test_utils.log_detail(None,'Now will test whether subscriptions come on calculated items change','')
	addr=dcs_opc_ua_elmb.node_obj_addr('can0',1,'sqrt_3')
	addr_s = dcs_opc_ua_elmb.addr_to_str(addr)
	try:
		dcs_opc_ua_elmb.client.createMonitoredData([addr], None, None, None, [subscription_verification])
		# Now set analog input to see convenient value (set it to 4 because sqrt(4) is 2 ;-) ), force Synch() and see if proper subscription came
		dcs_conn_log.sim_set_analog_input('can0',1,3,4)
		dcs_conn_log.call_method_bus('can0','Synch')
		time.sleep(5)
		if subscription_came_flag:
			dcs_test_utils.log_detail(True,'Subscription came', addr_s)
		else:
			dcs_test_utils.log_detail(False,'Subscription didnt come', addr_s)
	except Exception as e:
		dcs_test_utils.log_detail(False, 'Exception: '+str(e), addr_s)
	
	
	

finally:
	dcs_test_utils.kill_server()

dcs_test_utils.create_detailed_html ()
dcs_test_utils.create_summary_pickle()
