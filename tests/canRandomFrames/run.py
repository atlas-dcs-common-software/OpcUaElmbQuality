#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
from datetime import datetime
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings

paramMaxTestingTime=60.0

time.sleep(30)

parser=dcs_test_utils.get_standard_parser()
parser.add_option ("--max_testing_time", dest="max_testing_time", default="60")
dcs_test_utils.parse_options(parser)
paramMaxTestingTime=float(dcs_test_utils.options.max_testing_time)
dcs_test_utils.start_server()
time.sleep(30)
try:
	tstart = datetime.now()	
	# Essentially: every 1 second (up to paramMaxTestingTime) check if the server is still alive
	while True:
		tdelta = datetime.now() - tstart
		if not dcs_test_utils.is_server_running():
			dcs_test_utils.log_detail(False, 'Server died after about '+str(tdelta.seconds)+ ' seconds', '')
			break
		if tdelta.seconds > paramMaxTestingTime:
			dcs_test_utils.log_detail(True, 'Server was running without crash for about '+str(tdelta.seconds)+' seconds','')
			break	
		time.sleep(1)
finally:
	
	dcs_test_utils.kill_server()


dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()
