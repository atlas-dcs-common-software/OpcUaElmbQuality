#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from optparse 		    import OptionParser

nodeNotDelayed=1
nodeSlightlyDelayed=2
nodeVeryDelayed=3

# expressed in seconds,huh
slightDelay=10
veryDelay=120

# For all handlers, the only parameter is of type ReadResult

treq1=''
tres1=''
treq2=''
tres2=''
tres3=''
tres4=''
result1_status=None
result1_value=''
result2_status=None
result2_value=''
result3_status=None
result3_value=''
result4_status=None
result4_value=''
def handler1(r):
	global result1_status
	global result1_value
	global tres1
	tres1=pyuaf.util.DateTime.now().toTime_t()
	print 'handler1  response status is '+str(r.overallStatus)
	if str(r.overallStatus)=='Good':
		result1_status=True
	else:
		result1_status=False
	print 'handler1 value is '+str(r.targets[0].data.value)
	result1_value = copy.deepcopy(str(r.targets[0].data.value))
	print 'handler1 quit'

def handler2(r):
	global result2_status
	global result2_value
	global tres2 
	tres2=pyuaf.util.DateTime.now().toTime_t()
	print 'handler2 response status is '+str(r.overallStatus)
	print 'handler2 value is '+str(r.targets[0].data.value)
	if str(r.overallStatus)=='Good':
		result2_status=True
	else:
		result2_status=False
	result2_value = copy.deepcopy(str(r.targets[0].data.value))

def handler3(r):
	global result3_status
	global tres3 
	tres3=pyuaf.util.DateTime.now().toTime_t()
	print 'handler3 response status is '+str(r.overallStatus)
#	print 'handler3 value is '+str(r.targets[0].data.value)
	if str(r.targets[0].status)=='Good':
		result3_status=True
	else:
		result3_status=False
	print 'handler3 quit'

def handler4(r):
	global result4_status
	global tres4 
	tres4=pyuaf.util.DateTime.now().toTime_t()
	print 'handler4 response status is '+str(r.overallStatus)
#	print 'handler4 value is '+str(r.targets[0].data.value)
	if str(r.targets[0].status)=='Good':
		result4_status=True
	else:
		result4_status=False

def verifySyncSlightlyDelayed():
	addr1 = dcs_opc_ua_elmb.node_obj_addr('can0',nodeSlightlyDelayed,'serialNumber')
	addr1_s = dcs_opc_ua_elmb.addr_to_str(addr1)
	try:
		res=dcs_opc_ua_elmb.client.read([addr1], 13, rc)
		print(str(res.overallStatus))
		print(str(res.targets[0].status))
		if str(res.overallStatus)!='Good':
			dcs_test_utils.log_detail(False,'(sync read) Status not good as expected',addr1_s)
		else:
			if str(res.targets[0].data.value)=='842150450':
				dcs_test_utils.log_detail(True,'OK(sync read) - value is correct',addr1_s)
			else:
				dcs_test_utils.log_detail(True,'Bad (sync read) - value is not correct:'+str(res.targets[0].data.value),addr1_s)
	except Exception as e:
		dcs_test_utils.log_detail(False,'Exception while read(): '+str(e), addr1_s)

def verifySyncVeryDelayed():
	addr1 = dcs_opc_ua_elmb.node_obj_addr('can0',nodeVeryDelayed,'serialNumber')
	addr1_s = dcs_opc_ua_elmb.addr_to_str(addr1)
	try:
		res=dcs_opc_ua_elmb.client.read([addr1], 13, rc)
		if str(res.targets[0].status)=='Good':
			dcs_test_utils.log_detail(False,'(sync read) Status not bad as expected: '+str(res.targets[0].status),addr1_s)
		else:
			# check value also
			dcs_test_utils.log_detail(True,'OK(sync read)--status bad as expected',addr1_s)
	except Exception as e:
		dcs_test_utils.log_detail(False,'Exception while read(): '+str(e), addr1_s)

dcs_test_utils.parse_options()
# Start OPC UA Server 
dcs_test_utils.start_server()

try:
	dcs_opc_ua_elmb.connect()	
#	dcs_conn_log.call_method_bus('can0','Start')
	time.sleep(5)


	

	# Override default read settings which have fairly strict limit on possible timeout
	rs=pyuaf.client.settings.ReadSettings	()
	rs.callTimeoutSec=3600.0
	rc=pyuaf.client.configs.ReadConfig()
	rc.serviceSettings=rs
	
	# First try ordinary read on something that we know is fine for sure
	verifySyncSlightlyDelayed()
	# Then try ordinary read on something that should fail 
	verifySyncVeryDelayed()


	# Note: serialNumber SDO has a big timeout, hwVersion SDO has a small timeout
	addr1 = dcs_opc_ua_elmb.node_obj_addr ('can0',nodeSlightlyDelayed,'serialNumber')
	addr1_s = dcs_opc_ua_elmb.addr_to_str(addr1)
	addr2 = dcs_opc_ua_elmb.node_obj_addr ('can0',nodeNotDelayed, 'serialNumber')
	addr2_s = dcs_opc_ua_elmb.addr_to_str(addr2)
	addr3 = dcs_opc_ua_elmb.node_obj_addr ('can0',nodeSlightlyDelayed, 'hwVersion')
	addr3_s = dcs_opc_ua_elmb.addr_to_str(addr3)
	addr4 = dcs_opc_ua_elmb.node_obj_addr ('can0',nodeVeryDelayed, 'serialNumber')
	addr4_s = dcs_opc_ua_elmb.addr_to_str(addr4)

	try:
		err=dcs_opc_ua_elmb.client.beginRead([addr1], 13, rc, None, handler1)
		print ('after beginRead 1,status='+str(err.overallStatus))
		treq1 = pyuaf.util.DateTime.now().toTime_t()
		time.sleep(1)

		err=dcs_opc_ua_elmb.client.beginRead([addr2], 13, rc, None, handler2)
		print ('after beginRead 2,status='+str(err.overallStatus))
		treq2 = pyuaf.util.DateTime.now().toTime_t()
		time.sleep(1)

		err=dcs_opc_ua_elmb.client.beginRead([addr3], 13, rc, None, handler3)
		print ('after beginRead 3,status='+str(err.overallStatus))
		treq3 = pyuaf.util.DateTime.now().toTime_t()
		time.sleep(1)

		err=dcs_opc_ua_elmb.client.beginRead([addr4], 13, rc, None, handler4)
		print ('after beginRead 4,status='+str(err.overallStatus))
		treq4 = pyuaf.util.DateTime.now().toTime_t()
		time.sleep(1)
	except Exception as e:
		print("Exception="+str(e))
		dcs_test_utils.log_detail(False,'beginRead() e='+str(e),'')

	#Wait some time until all async operation will have finished
	time.sleep(16)
	print 'h1 val ****** '+result1_value
	
	#now analyze
	#call1 was supposed to be successful
	print result1_status
	if result1_status==None:
		dcs_test_utils.log_detail(False,'Op1 handler never called!',addr1_s)
	else:
		if not result1_status:
			dcs_test_utils.log_detail(False,'Op1 bad status! expected good.',addr1_s)
		else:
			if result1_value=='842150450':
				dcs_test_utils.log_detail(True,'Op1 called, status and value good.',addr1_s)
			else:
				dcs_test_utils.log_detail(False,'Op1 called, status is good but value incorrect:.'+result1_value,addr1_s)

	# call2 was supposed to be successful 
	if result2_status==None:
		dcs_test_utils.log_detail(False,'Op2 handler never called!',addr2_s)
	else:
		if not result2_status:
			dcs_test_utils.log_detail(False,'Op2 bad status! expected good.',addr2_s)
		else:
			if result2_value=='825307441':
				dcs_test_utils.log_detail(True,'Op2 called, status and value good.',addr2_s)
			else:
				dcs_test_utils.log_detail(False,'Op2 called, status is good but value incorrect:.'+result2_value,addr2_s)
	# call3 was supposed to fail (because in the meantime semaphore for call1 was occupied)
	if result3_status==None:
		dcs_test_utils.log_detail(False,'Op3 handler never called!',addr3_s)
	else:
		if result3_status:
			dcs_test_utils.log_detail(False,'Op3 good status! expected bad.,value='+result3_value,addr3_s)
		else:
			dcs_test_utils.log_detail(True,'Op3 called, status is bad,value='+result3_value,addr3_s)
	# call4 was supposed to fail (because its sdo_delay was soooooo big)
	if result4_status==None:
		dcs_test_utils.log_detail(False,'Op4 handler never called!',addr4_s)
	else:
		if result4_status:
			dcs_test_utils.log_detail(False,'Op4 good status! expected bad.,value='+result4_value,addr4_s)
		else:
			dcs_test_utils.log_detail(True,'Op4 called, status is bad,value='+result4_value,addr4_s)

	# Now analyze timing relations
	# response of call2 shall have arived before response of call1 -- because node of call1 is not delayed , and the semaphore shall be per node
	
	if tres2<tres1:
		dcs_test_utils.log_detail(True, 'Resp2 came before Resp1 -- semaphore didnt block it' ,'')
	else:
		dcs_test_utils.log_detail(False, 'Resp2 came AFTER Resp1 -- semaphore  blocked it ??' ,'')
	# response of call3 shall have arrived before response of call1 -- because call3's SDO has a small timeour
	if tres3>tres1:
		dcs_test_utils.log_detail(True, 'Resp3 came after Resp1 -- semaphore block OK', '')
	else:
		dcs_test_utils.log_detail(False, 'Read3 came BEFORE Resp1 -- semaphore didnt block','')

	# print some summary
	print('Times relative to request of (1):')
	print('resp1  '+str(tres1-treq1))
	print('resp2  '+str(tres2-treq1))
	print('treq3  '+str(treq3-treq1))
	print('tres3  '+str(tres3-treq1))
	



finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

