#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_server_config
import dcs_sim_config
import dcs_conn_log
from pyuaf.util             import Address, NodeId
import pyuaf.util.opcuastatuscodes
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings, CreateMonitoredDataSettings
import dcs_sim_connection
import pdb
from pyuaf.client.requests import CreateMonitoredDataRequest, CreateMonitoredDataRequestTargetVector, CreateMonitoredDataRequestTarget

#TEST_PARAMS
paramNumBuses=1
paramNumElmbsPerBus=16
paramNumAnalogChannelsPerElmb=64
paramNumIterations=2
paramDontLogGoodValues=False

counter=0
counterGoodValue=0
counterGoodStatus=0
# listAddresses
addresses = []
expectedStatus = None
expectedValue = None

generalLock=thread.allocate_lock()

def testValuesByRead(iteration,sync_times, stateExpected):
	expected=iteration
	all=paramNumBuses*paramNumElmbsPerBus*paramNumAnalogChannelsPerElmb
	ctr=0
	for bus in range(0,paramNumBuses):
		for elmb in range(1,paramNumElmbsPerBus+1):
			for channel in range(0,paramNumAnalogChannelsPerElmb):
				print '\rProgress: '+str(ctr)+' / '+str(all)+' ...'
				ctr=ctr+1
				try:
					addr_s = 'can'+str(bus)+'.elmb'+str(elmb)+'.ai_'+str(channel)
					addr = dcs_opc_ua_elmb.addr(addr_s)
					result = dcs_opc_ua_elmb.client.read([ addr ])
				#	print result.overallStatus
					#import pdb; pdb.set_trace()
					obtainedStatus=result.targets[0].status.opcUaStatusCode()
					
					if (obtainedStatus !=stateExpected):
						dcs_test_utils.log_detail (False,'Status is not as expected: obtained:'+str(obtainedStatus)+' expected was:'+str(stateExpected),addr_s)
						#import pdb; pdb.set_trace()
					else:
						result_v=result.targets[0].data.value
				#		print("The counter value is %d" %result_v)
						if result_v!=expected:
							dcs_test_utils.log_detail (False,'Read value='+str(result_v)+' expected='+str(expected),addr_s)
						else:
							if not paramDontLogGoodValues:
								dcs_test_utils.log_detail (True,'Value obtained is good',addr_s) 
					if sync_times != None:
						time_diff = result.targets[0].sourceTimestamp.toTime_t() - sync_times[bus]
						if time_diff>=0 and time_diff<120:
							if not paramDontLogGoodValues:
								dcs_test_utils.log_detail(True,'Source timestamp fine,time_diff='+str(time_diff),addr_s)
						else:
							dcs_test_utils.log_detail(False,'Source timestamp fail,time_diff='+str(time_diff),addr_s)
						time_diff = result.targets[0].serverTimestamp.toTime_t() - sync_times[bus]
						if time_diff>=0 and time_diff<120:
							if not paramDontLogGoodValues:
								dcs_test_utils.log_detail(True,'Server timestamp fine,time_diff='+str(time_diff),addr_s)
						else:
							dcs_test_utils.log_detail(False,'Server timestamp fail,time_diff='+str(time_diff),addr_s)
				except Exception as e:
					dcs_test_utils.log_detail (False,'Exception '+str(e),'')
					print sys.exc_info()[1].message
					raise e

def dataChanged(d):
	global counter,counterGoodValue,counterGoodStatus,generalLock
	with generalLock:
		counter = counter+1
		if d.status.opcUaStatusCode()==expectedStatus:
			counterGoodStatus = counterGoodStatus+1
		else:
			#pdb.set_trace()
			pass
		if d.data.value == expectedValue:
			counterGoodValue = counterGoodValue+1

parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="64")
parser.add_option ("-b", "--buses", dest="buses", default="2")
parser.add_option ("--iterations", dest="iterations", default="2")
parser.add_option ("--dont_log_good_values", dest="dont_log_good_values", action="store_true") 
dcs_test_utils.parse_options(parser)
paramNumElmbsPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=int(dcs_test_utils.options.buses)
paramNumIterations=int(dcs_test_utils.options.iterations)
paramDontLogGoodValues=dcs_test_utils.options.dont_log_good_values
paramLogFrames=dcs_test_utils.options.log_frames
# Start OPC UA Server 
dcs_server_config.create_basic_server_config(paramNumBuses,paramNumElmbsPerBus,0,0,'ELMB_with_states')
dcs_sim_config.create_basic_sim_config(paramNumBuses,paramNumElmbsPerBus,'','',paramLogFrames)
run_options={}
if dcs_test_utils.options.valgrind:
	run_options={'valgrind':True}
dcs_test_utils.start_server(run_options)

try:
	time.sleep(10)
	dcs_opc_ua_elmb.connect()	
	
	# prepare addresses
	for bus in range(0,paramNumBuses):
		for elmb in range(1,paramNumElmbsPerBus+1):
			for channel in range(0,paramNumAnalogChannelsPerElmb):
				addresses.append(dcs_opc_ua_elmb.addr('can'+str(bus)+'.elmb'+str(elmb)+'.ai_'+str(channel)))
	# install handler
# 	ss = CreateMonitoredDataSettings()
# 	sc = CreateMonitoredDataConfig()
	
	
	
	r=dcs_opc_ua_elmb.client.createMonitoredData( addresses, None, None, None, [dataChanged]*len(addresses) )
	
	
# 	targets = CreateMonitoredDataRequestTargetVector (  )
# 	for a in addresses:
# 		target = CreateMonitoredDataRequestTarget()
# 		target.address = a
# 		target.sa
# 	
# 	request = CreateMonitoredDataRequest( target ) 
	
	if not r.overallStatus.isGood():
		raise Exception ("Couldn't create a monitored item ")
	
	# check what was sampling interval
	for a in r.targets:
		if a.revisedSamplingIntervalSec != 0:
			raise Exception("Revised sampling interval is not zero: "+str(a.revisedSamplingIntervalSec))
	
	for bus in range(0,paramNumBuses):
		dcs_conn_log.call_method_bus('can'+str(bus),'Start')
	
	time.sleep(10)
	
	for iter in range(1,paramNumIterations+1):
		dcs_test_utils.log_detail(None,'Beginning iteration '+str(iter),'')
		
		counter = 0
		counterGoodStatus = 0
		counterGoodValue = 0
		
		# when iter number is even -> state is Good
		
		stateForChannel = iter % 2
		stateExpected = None
		if stateForChannel==0:
			stateExpected = pyuaf.util.opcuastatuscodes.OpcUa_Good
		else:
			stateExpected = -pyuaf.util.opcuastatuscodes.OpcUa_Bad
		expectedStatus = stateExpected
		expectedValue = iter
		
		
		
		# Set status of a channel
		for bus in range(0,paramNumBuses):
			for elmb in range(1,paramNumElmbsPerBus+1):
				for ch in range(0,64):
					dcs_sim_connection.set_conversion_flag('can'+str(bus), elmb, ch, str(stateForChannel))
		
		
		sync_times=[]
		# call sync on all buses and remember when it was issued
		for bus in range(0,paramNumBuses):
			dcs_conn_log.call_method_bus('can'+str(bus),'Synch')
			sync_times.append(pyuaf.util.DateTime.now().toTime_t())
			
		# give some time after sync -- after one sync on every bus would send 64*paramNumElmbsPerBus TPDO3 (every about 100bits length) 
		# so we should divide that by bus speed and multiply by some common factor eg.2
		timeFromBusSpeed=2*64.0*paramNumElmbsPerBus*100.0 / 125000
		# common factor * number channels / conversion Rate
		timeFromConversionRate=2*64/100.0
		time.sleep(max(timeFromBusSpeed,timeFromConversionRate)+1)
		
		with generalLock:
			time.sleep(1) #  enable to run the debugger from the thread

		testValuesByRead(iter,sync_times, stateExpected)
		numCh = paramNumBuses*paramNumElmbsPerBus*paramNumAnalogChannelsPerElmb
		if counter == numCh:
			dcs_test_utils.log_detail(True,'Overall number of notifications received good ('+str(counter)+')', '')
		else:
			dcs_test_utils.log_detail(False,'Overall number of notifications bad (got '+str(counter)+' expected '+str(numCh)+')', '')
		if counterGoodStatus == numCh:
			dcs_test_utils.log_detail(True,'Number of notifications with good status is good ('+str(counterGoodStatus)+')', '')
		else:
			dcs_test_utils.log_detail(False,'Number of notifications with good status is bad (got '+str(counterGoodStatus)+' expected '+str(numCh)+')', '')	
		if counterGoodValue == numCh:
			dcs_test_utils.log_detail(True,'Number of notifications with good value is good ('+str(counterGoodValue)+')', '')
		else:
			dcs_test_utils.log_detail(False,'Number of notifications with good value is bad (got '+str(counterGoodValue)+' expected '+str(numCh)+')', '')	
	dcs_test_utils.log_detail(None,'Now trying per-node Synch','')


### I forgot sync doesn't make sense per node .... but maybe RTR would be fine?
#
#	# now call sync individually on every node
#	for bus in range (0,paramNumBuses):
#		for node in range(0,paramNumElmbsPerBus):
#			dcs_conn_log.call_method_node('can'+str(bus),node,'Synch')
#	time.sleep(5)
#	testValuesByRead(paramNumIterations+1,None)
	
	
finally:
	dcs_test_utils.kill_server()

print "Counter was called: "+str(counter)+" times "

dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

