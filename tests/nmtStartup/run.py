#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_sim_connection
import dcs_conn_log
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings

# DESCRIPTION
# Server is configured such that it is possible to verify all NMT Startup options.
# can0 -> started upon server start (initial state: preop(default) )
# can1 -> stopped upon server start (initial state: preop(default) )
# can2 -> reseted upon server start (initial state: started ) --> bootup time changes due to reset
# can3 -> preoped uppon server start (initial state: started ) --> bootup time doesn't change
# can4:
#	elmb1 -> started
#	elmb2 -> stopped
#	elmb3 -> reset
#	elmb4 -> preoperational

# Verification
def verifyNode(bus,node,expectedState):
	try:
		state=dcs_sim_connection.get_node_nmt_state(bus,node)
		if (state==expectedState):
			dcs_test_utils.log_detail(True,'OK(sim) '+state,bus+' '+str(node))
		else:
			dcs_test_utils.log_detail(False,'(sim)Read='+state+' Expected='+expectedState,bus+' '+str(node))
	except Exception as e:
		dcs_test_utils.log_detail(False,'(sim)Read EXCEPTION '+str(e),bus+' '+str(node));
	
dcs_test_utils.parse_options() 
# Start OPC UA Server 
dcs_test_utils.start_server()
dcs_sim_connection.open_connection()
try:
	dcs_opc_ua_elmb.connect()
	time.sleep(5)
	verifyNode ('can0',1,'operational')
	verifyNode ('can1',1,'stopped')
	verifyNode ('can2',1,'preoperational')
	verifyNode ('can3',1,'preoperational')
	verifyNode ('can4',1,'operational')
	verifyNode ('can4',2,'stopped')
	verifyNode ('can4',3,'preoperational')
	verifyNode ('can4',4,'preoperational')
finally:
	
	dcs_test_utils.kill_server()


dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()
