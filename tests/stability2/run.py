#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_server_config
import dcs_sim_config
import dcs_conn_log
import random
import traceback
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives import *
from pyuaf.util.statuscodes import *


# once it shall move to time-based operation
paramIterations=2000
paramSleep=0.005
paramMaxParallelOps=1
paramRunningTime=60
currentWriteThreads=0

paramStepsAtOnce=0
globalLock=None
quitDesired=False
callbackInvocations=0

log_items_in_processing = []
log_items_processed = []

def createServerConfigFile(buses,nodes,nodeguard,sync):
	fname='OPCUACANOpenServer.xml.source'
	dcs_server_config.write_prologue(fname,'ELMB_local')
	f=open(fname,'a')
	for bus in range(0,buses):
		bus_name='can'+str(bus)
		f.write ('<CANBUS speed="'+str(dcs_server_config.bitrate)+'" port="'+bus_name+'" type="'+dcs_server_config.connection_type+'" name="'+bus_name+'"  >\n');
		f.write ('<NODEGUARD interval="'+str(nodeguard)+'"/>\n')
		f.write ('<SYNC interval="'+str(sync)+'"/>\n')
		for node in range(1,nodes+1):
			f.write('<NODE type="ELMB" name="elmb'+str(node)+'" nodeid="'+str(node)+'">\n')
			for ch in range(0,32):
				f.write('<ITEM name="item'+str(ch)+'" value="TPDO3.Value_'+str(ch)+'" /> \n')
			for ch in range(0,32):
				f.write('<ITEM name="item'+str(32+ch)+'" value="TPDO3.Value_'+str(2*ch)+'*TPDO3.Value_'+str(2*ch+1)+'" /> \n')
			f.write('</NODE> \n')
		f.write('</CANBUS>\n')
	f.close()	
	dcs_server_config.write_epilogue(fname)


op_READ=1
op_WRITE=2	
	
def writeCallback(r):
	print 'writeCallback r='+str(r)	
	
def genericCallbackNoteNotGood(r):
	global globalLock
	global currentWriteThreads
	global callbackInvocations
	with globalLock:
		print ('obtained callback for num items='+str(len(r.targets)))
		callbackInvocations=callbackInvocations+1
		try:
			if r.targets[0].status != Good:
				dcs_test_utils.log_detail(False, 'Status not good: '+str(r.targets[0].status), '')
		except Exception as e:
			dcs_test_utils.log_detail(False, 'Exception: '+str(e))
		finally:
			currentWriteThreads=currentWriteThreads-1

		
	

		
		
def thread_read_things (list_obj,op,val=None):
	global globalLock
	global currentWriteThreads

	addr=None
	
	
	
	while True:
		try:
			with globalLock:
				_quitDesired = quitDesired
			if _quitDesired:
				break

			# here begins preparation for one operation
			num_items_to_pick = random.randint(1,len(list_obj))
			string_addresses=[]
			for i in range(0,num_items_to_pick):
				string_addresses.append(list_obj[random.randint(0,len(list_obj)-1)])
			
			
			opcua_addresses=[]
			print 'These things will be read:'
			for sa in string_addresses:
				print sa
				opcua_addresses.append(dcs_opc_ua_elmb.addr(sa))
			
# 			addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus), node, obj)	
# 			res=None
			# make sure than no more operations than allowed is there
			
			waitCtr=0
			while True:
				with globalLock:
					if currentWriteThreads < paramMaxParallelOps: 
						currentWriteThreads = currentWriteThreads+1
						break
			#	print 'Have to wait: current writes='+str(myCurrentWriteThreads)
				waitCtr = waitCtr+1
				if waitCtr > 150:
					print 'Already waiting 5 seconds for going on ....'
					waitCtr=0
					with globalLock:
						dcs_test_utils.log_detail(False,'Waiting for long time...',dcs_opc_ua_elmb.addr_to_str(addr))
				time.sleep(0.2)
			
			res=dcs_opc_ua_elmb.client.beginRead (opcua_addresses, 13, None, None, genericCallbackNoteNotGood)
			print 'Result was '+str(res.overallStatus)
# 			if op==op_READ:
# 				res=dcs_opc_ua_elmb.client.beginRead([addr],13,None,None,genericCallbackNoteNotGood)
# 			elif op==op_WRITE:
# 				print 'writing address: '+dcs_opc_ua_elmb.addr_to_str(addr)
# 				res=dcs_opc_ua_elmb.client.beginWrite([addr],[val],13,None,None,genericCallbackNoteNotGood)
				
			
			time.sleep(paramSleep)
		except Exception as e:
			# this exception has to be handled here
			traceback.print_exc()
		
		
def thread_write_things (list_obj):
	global globalLock
	global currentWriteThreads

	addr=None
	
	
	
	while True:
		try:
			with globalLock:
				_quitDesired = quitDesired
			if _quitDesired:
				break

			# here begins preparation for one operation
			num_items_to_pick = random.randint(1,len(list_obj))
			string_addresses=[]
			values=[]
			for i in range(0,num_items_to_pick):
				which_item = random.randint(0,len(list_obj)-1)
				string_addresses.append(list_obj[which_item]['a'])
				values.append(list_obj[which_item]['v'])
			
			
			opcua_addresses=[]
			print 'These things will be written:'
			for sa in string_addresses:
				print sa
				opcua_addresses.append(dcs_opc_ua_elmb.addr(sa))
			
# 			addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus), node, obj)	
# 			res=None
			# make sure than no more operations than allowed is there
			
			waitCtr=0
			while True:
				with globalLock:
					if currentWriteThreads < paramMaxParallelOps: 
						currentWriteThreads = currentWriteThreads+1
						break
			#	print 'Have to wait: current writes='+str(myCurrentWriteThreads)
				waitCtr = waitCtr+1
				if waitCtr > 50:
					print 'Already waiting 5 seconds for going on ....'
					waitCtr=0
					with globalLock:
						dcs_test_utils.log_detail(False,'Waiting for long time...',dcs_opc_ua_elmb.addr_to_str(addr))
				time.sleep(0.1)
			
			res=dcs_opc_ua_elmb.client.beginWrite (opcua_addresses, values, 13, None, None, genericCallbackNoteNotGood)
			print 'Result was '+str(res.overallStatus)
			
			l = {'requestHandle':res.requestHandle, 'a':string_addresses, 't':'w'}
			with globalLock:
				log_items_in_processing.append(l)
# 			if op==op_READ:
# 				res=dcs_opc_ua_elmb.client.beginRead([addr],13,None,None,genericCallbackNoteNotGood)
# 			elif op==op_WRITE:
# 				print 'writing address: '+dcs_opc_ua_elmb.addr_to_str(addr)
# 				res=dcs_opc_ua_elmb.client.beginWrite([addr],[val],13,None,None,genericCallbackNoteNotGood)
				
			
			time.sleep(paramSleep)
		except Exception as e:
			# this exception has to be handled here
			traceback.print_exc()
		
	
time.sleep(5)	
globalLock = thread.allocate_lock()
parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="1")
parser.add_option ("-b", "--buses", dest="buses", default="1")
parser.add_option ("--delay", dest="delay", default="0.01")
parser.add_option ("--max_parallel_ops", dest="max_parallel_ops", default="3")
parser.add_option ("--running_time", dest="running_time", default="60")

parser.add_option ("--do_write_sdo", dest="do_write_sdo", action="store_true")
parser.add_option ("--do_read_sdo", dest="do_read_sdo", action="store_true")
parser.add_option ("--do_write_pdo", dest="do_write_pdo", action="store_true")
parser.add_option ("--do_read_pdo", dest="do_read_pdo", action="store_true")
parser.add_option ("--do_write_segmented_sdo", dest="do_write_segmented_sdo", action="store_true")
parser.add_option ("--do_read_segmented_sdo", dest="do_read_segmented_sdo", action="store_true")
parser.add_option ("--do_read_calc_items", dest="do_read_calc_items", action="store_true")

dcs_test_utils.parse_options(parser)
paramNumElmbsPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=int(dcs_test_utils.options.buses)
paramSleep=float(dcs_test_utils.options.delay)
paramMaxParallelOps=int(dcs_test_utils.options.max_parallel_ops)
paramRunningTime=int(dcs_test_utils.options.running_time)
# Start OPC UA Server 

# Create the config files
createServerConfigFile(paramNumBuses,paramNumElmbsPerBus, 5000, 5000)
time.sleep(5)
dcs_sim_config.create_basic_sim_config(paramNumBuses,paramNumElmbsPerBus,'','',True)
dcs_test_utils.start_server()

try:
	time.sleep(70)
	dcs_opc_ua_elmb.connect()	
	
	for bus in range(0,paramNumBuses):
		dcs_conn_log.call_method_bus('can'+str(bus),'Start')

	list_items_to_read=[]
	list_items_to_write=[]

	if dcs_test_utils.options.do_read_pdo:
		list_read_pdo=[]
		for i in range(0,2):
			n='can0.elmb1.TPDO3.Value_'+str(i)
			list_items_to_read.append(n)
	
	if dcs_test_utils.options.do_read_calc_items:
		list_read_item=[]
		for i in range(0,10):
			list_read_item.append('item'+str(i))
			# TODO

	
	if dcs_test_utils.options.do_read_segmented_sdo:
		for b in range(0, paramNumBuses):
			for n in range(1, paramNumElmbsPerBus+1):
				list_items_to_read.append('can'+str(b)+'.elmb'+str(n)+'.Ireg_wr')
	
	if dcs_test_utils.options.do_write_segmented_sdo:
		for b in range(0, paramNumBuses):
			for n in range(1, paramNumElmbsPerBus+1):
				list_items_to_write.append({'a':'can'+str(b)+'.elmb'+str(n)+'.Ireg_wr', 'v':ByteString(bytearray(random.randint(1,100))) })


	if dcs_test_utils.options.do_write_pdo:
		for b in range(0, paramNumBuses):
			for n in range(1, paramNumElmbsPerBus+1):
				list_items_to_write.append({'a':'can'+str(b)+'.elmb'+str(n)+'.RPDO1.do_write', 'v':UInt16(random.randint(0,65536)) })

	
	if dcs_test_utils.options.do_write_sdo:
		for b in range(0, paramNumBuses):
			for n in range(1, paramNumElmbsPerBus+1):
				list_items_to_write.append({'a':'can'+str(b)+'.elmb'+str(n)+'.doInitHigh', 'v':Byte(random.randint(0,255)) })

	
	if dcs_test_utils.options.do_read_sdo:
		for b in range(0, paramNumBuses):
			for n in range(1, paramNumElmbsPerBus+1):
				list_items_to_read.append('can'+str(b)+'.elmb'+str(n)+'.hwVersion')
	
	for i in list_items_to_read:
		print 'to read: '+i
	
	if len(list_items_to_read)>0:
		thread.start_new_thread(thread_read_things,(list_items_to_read,op_READ,))
	if len(list_items_to_write)>0:
		thread.start_new_thread(thread_write_things,(list_items_to_write,))
	
	time.sleep(paramRunningTime)
	with globalLock:
		quitDesired=True
	time.sleep(2)
	
	if callbackInvocations>0:
		dcs_test_utils.log_comment('# callback invocations:'+str(callbackInvocations))
	else:
		dcs_test_utils.log_detail(False,'#callback invocations is zero','')
	
	
finally:
	dcs_test_utils.kill_server()

print 'Callback was invoked '+str(callbackInvocations)+' times'

dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

