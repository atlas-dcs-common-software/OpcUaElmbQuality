#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
import dcs_sim_connection
import dcs_server_config
import dcs_sim_config
import random
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from optparse 		    import OptionParser
from pyuaf.util.primitives  import Byte,UInt32 
# Start OPC UA Server 
# dcs_test_utils.start_server()


paramNumBuses=1
paramNumNodesPerBus=12
paramMaxParallelWrites=16
max_len=1000
reg='Ireg_wr'
def generateRandomBytes(len):
	test_bytes=[]
	for i in range(0,len):
		test_bytes.append(Byte(random.randint(0,255)))
	return test_bytes

wc=None
rc=None

def compare(test_bytes,r):
	errors=0
	for i in range(0,len(test_bytes)):
		if test_bytes[i].value != r.value[i]:
			print 'differs, got:'+str(test_bytes[i].value)+' second is'+str(r[i].value)
			errors=errors+1
	return errors

def toByteString(bytes):
	r=bytearray(b"")
	for b in bytes:
		r.append(b.value)
	bytestring=pyuaf.util.primitives.ByteString(r)
	return bytestring

def verifySynchronous():
	for bus in range(0,paramNumBuses):
		for node in range(1, paramNumNodesPerBus+1):
			len=3
			while len<max_len:
				bytes=generateRandomBytes(len)
				addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus),str(node),reg)
				addr_s = dcs_opc_ua_elmb.addr_to_str(addr)
				print 'Now trying to write: '+str(bytes) 
				res=dcs_opc_ua_elmb.client.write([addr],[ toByteString(bytes) ], 13, wc)
				if str(res.overallStatus) != 'Good':
					dcs_test_utils.log_detail(False, 'write error not good: '+str(res.targets[0].status), addr_s) 
				try:
					err=dcs_sim_connection.compare_by_chunks('can'+str(bus), str(node), reg, bytes)
					if err>0:
						dcs_test_utils.log_detail(False,'Compare: errors='+str(err)+' len='+str(len), addr_s)
					else:
						dcs_test_utils.log_detail(True,'OK len='+str(len), addr_s)
				except Exception as e:
					dcs_test_utils.log_detail(False,'When reading from simulator to compare: Exception:'+str(e), addr_s) 
				# now compare by reading

				addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus),str(node),'Ireg_rd')
				try:
					res=dcs_opc_ua_elmb.client.read([addr], 13, rc)
					if str(res.overallStatus)!='Good':
						dcs_test_utils.log_detail(False,'Sync read(): status not good', addr_s)
					else:
						if compare(bytes,res.targets[0].data)==0:
							dcs_test_utils.log_detail(True,'Sync read(); data read-back correct.', addr_s)
						else:	
							dcs_test_utils.log_detail(False,'Sync read(); data read-back incorrect.', addr_s)
							print res.targets[0].data.value
				except Exception as e:
					dcs_test_utils.log_detail(False,'Sync read() Exception: '+str(e), addr_s)
				len = int(len*1.4142)

asynchronousLock=None
currentWriteThreads=0
def asynchronousWriteCallback(r):
	global asynchronousLock
	global currentWriteThreads
	try:
		with asynchronousLock:
			print 'currentWriteThreads='+str(currentWriteThreads)+' will decrease'
			currentWriteThreads = currentWriteThreads-1
		print 'asynchronousWriteCallback finished'
	except Exception as e:
		print str(e)
	
	

def verifyAsynchronous():
	global asynchronousLock
	global currentWriteThreads
	asynchronousLock = thread.allocate_lock()
	length=3
	sent_bytes=[]
	while length<max_len:
		print 'current len='+str(length)
		bytes_by_buses=[]
		for bus in range(0,paramNumBuses):
			bytes_by_nodes=[]
			for node in range(1, paramNumNodesPerBus+1):
				bytes=generateRandomBytes(length)
				bytes_by_nodes.append(bytes)
				addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus),str(node),reg)
				addr_s = dcs_opc_ua_elmb.addr_to_str(addr)
				# here check for possibility of running another one

				while True:
					myCurrentWriteThreads=0
					asynchronousLock.acquire()
					myCurrentWriteThreads = currentWriteThreads
					asynchronousLock.release()
					if myCurrentWriteThreads < paramMaxParallelWrites: break
					print 'Have to wait: current writes='+str(myCurrentWriteThreads)
					time.sleep(0.1)
				asynchronousLock.acquire()
				currentWriteThreads = currentWriteThreads+1
				asynchronousLock.release()
				print ('now writing: '+addr_s)
				res=dcs_opc_ua_elmb.client.beginWrite([addr],[ toByteString(bytes) ], 13, None, None, asynchronousWriteCallback)
				#res=dcs_opc_ua_elmb.client.beginRead([addr], 13, None, None, asynchronousWriteCallback)
				print ('after calling beginWrite result is '+str(res.overallStatus))
			bytes_by_buses.append(bytes_by_nodes)
		
		# wait until everything transfered
		while True:
			myCurrentWriteThreads=0
			asynchronousLock.acquire()
			myCurrentWriteThreads = currentWriteThreads
			asynchronousLock.release()
			if myCurrentWriteThreads == 0: break
			print 'Have to wait: current writes='+str(myCurrentWriteThreads)
			time.sleep(0.1)
		time.sleep(1)
		# okay once everything is written for this given length
		for bus in range(0,paramNumBuses):
			print 'len(bytes_by_buses)='
			print len(bytes_by_buses)
			for node in range(1, paramNumNodesPerBus+1):
				addr_s = str(bus)+' '+str(node)
				print 'len(bytes_by_nodes)'+str(len(bytes_by_buses[bus]))
				err=dcs_sim_connection.compare_by_chunks('can'+str(bus), str(node), reg, (bytes_by_buses[bus])[node-1])
				if err>0:
					dcs_test_utils.log_detail(False,'Compare: errors='+str(err)+' len='+str(length), addr_s)
				else:
					dcs_test_utils.log_detail(True,'OK len='+str(length), addr_s)
		length = int(length*1.4142)


parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="64")
parser.add_option ("-b", "--buses", dest="buses", default="2")
parser.add_option ("-l", "--max_len", dest="max_len", default="1000")
parser.add_option ("--sync_interval", dest="sync_interval", default="0")
parser.add_option ("--ng_interval", dest="ng_interval", default="0")
parser.add_option ("--sync_writes", dest="sync_writes", action="store_true")
parser.add_option ("--async_writes", dest="async_writes", action="store_true")
parser.add_option ("--max_parallel_writes", dest="max_parallel_writes", default="16")
parser.add_option ("--segmented_sdo_delay", dest="segmented_sdo_delay", default="0.05")
dcs_test_utils.parse_options(parser)
paramNumNodesPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=int(dcs_test_utils.options.buses)
paramMaxParallelWrites=int(dcs_test_utils.options.max_parallel_writes)
paramSegmentedSdoDelay=dcs_test_utils.options.segmented_sdo_delay
max_len=int(dcs_test_utils.options.max_len)
syncInterval=int(dcs_test_utils.options.sync_interval)
ngInterval=int(dcs_test_utils.options.ng_interval)
# Start OPC UA Server 
dcs_server_config.create_basic_server_config(paramNumBuses,paramNumNodesPerBus,ngInterval,syncInterval,'mdt')
dcs_sim_config.create_basic_sim_config(paramNumBuses,paramNumNodesPerBus,'',' segmentedSdoDelay="'+paramSegmentedSdoDelay+'" ')
dcs_test_utils.start_server()
time.sleep(15)
try:
	dcs_opc_ua_elmb.connect()	
	dcs_sim_connection.open_connection()
	dcs_opc_ua_elmb.client.write([Address(NodeId(2000,2), dcs_opc_ua_elmb.server_uri)], [UInt32(255)]) 

	# start all elmbs on all buses
	for bus in range(0,paramNumBuses):
		dcs_conn_log.call_method_bus('can'+str(bus),'Start')

	ws=pyuaf.client.settings.WriteSettings	()
	ws.callTimeoutSec=10.0
	wc=pyuaf.client.configs.WriteConfig()
	wc.serviceSettings=ws
	
	rs=pyuaf.client.settings.ReadSettings	()
	rs.callTimeoutSec=10.0
	rc=pyuaf.client.configs.ReadConfig()
	rc.serviceSettings=rs

	if dcs_test_utils.options.sync_writes:
		verifySynchronous()
	if dcs_test_utils.options.async_writes:
		verifyAsynchronous()


finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

