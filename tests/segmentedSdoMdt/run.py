#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
import dcs_sim_connection
import dcs_server_config
import dcs_sim_config
import random
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from optparse 		    import OptionParser
from pyuaf.util.primitives  import Byte 
# Start OPC UA Server 
# dcs_test_utils.start_server()


paramNumBuses=1
paramNumNodesPerBus=12
paramMaxParallelWrites=16
paramSegmentedSdoDelay=0
max_len=1000
reg='Ireg_wr'

# reference bytes
Ireg_1=u"00,20,C3,FF,FF,FF"
Dreg_1=u"01,69,25,F0,01,00,00,00,00,00,C0,08,00,00,00,00,00,00,80,06,E1,29,F8,20,03,00,00,C0,FF,3F,04,02,41,00,4B,00,32,00,00,C0,08,00,00,80,06,E1,E9,01"
Ireg_2=Ireg_1
Dreg_2=Dreg_1
Ireg_3=Ireg_1
Dreg_3=u"01,69,25,F0,01,00,00,00,00,00,C0,08,00,00,00,00,00,00,80,06,E1,29,F8,20,03,00,00,C0,FF,3F,05,02,41,00,01,00,32,00,00,C0,08,00,00,80,06,E1,E9,01"
Ireg_4=Ireg_1
Dreg_4=u"""0B 44 04 E1 01 6E 40 06 BF 0D 00 E0 9D 07 EB 0D B0 FB BB D7 FF F1 FF FF 4F\
10  1E  E0  06  64  F0  DB 00  00  DE  79  B0  DE  00  BB  BF\
7B  FD  1F  FF  FF  FF  04 E1  01  6E  40  06  BF  0D  00  E0\
9D  07  EB  0D  B0  FB  BB D7  FF  F1  FF  FF  4F  10  1E  E0\
06  64  F0  DB  00  00  DE 79  B0  DE  00  BB  BF  7B  FD  1F\
FF  FF  FF  04  E1  01  6E 40  06  BF  0D  00  E0  9D  07  EB\
0D  B0  FB  BB  D7  FF  F1 FF  FF  4F  10  1E  E0  06  64  F0\
DB  00  00  DE  79  B0  DE 00  BB  BF  7B  FD  1F  FF  FF  FF\
04  E1  01  6E  40  06  BF 0D  00  E0  9D  07  EB  0D  B0  FB\
BB  D7  FF  F1  FF  FF  4F 10  1E  E0  06  64  F0  DB  00  00\
DE  79  B0  DE  00  BB  BF 7B  FD  1F  FF  FF  FF  04  E1  01\
6E  40  06  BF  0D  00  E0 9D  07  EB  0D  B0  FB  BB  D7  FF\
F1  FF  FF  4F  10  1E  E0 06  64  F0  DB  00  00  DE  79  B0\
DE  00  BB  BF  7B  FD  1F FF  FF  FF  04  E1  01  6E  40  06\
BF  0D  00  E0  9D  07  EB 0D  B0  FB  BB  D7  FF  F1  FF  FF\
4F  10  1E  E0  06  64  F0 DB  00  00  DE  79  B0  DE  00  BB\
BF  7B  FD  1F  FF  FF  FF 04  E1  01  6E  40  06  BF  0D  00\
E0  9D  07  EB  0D  B0  FB BB  D7  FF  F1  FF  FF  4F  10  1E\
E0  06  64  F0  DB  00  00 DE  79  B0  DE  00  BB  BF  7B  FD\
1F  FF  FF  FF  04  E1  01 6E  40  06  BF  0D  00  E0  9D  07\
EB  0D  B0  FB  BB  D7  FF F1  FF  FF  4F  10  1E  E0  06  64\
F0  DB  00  00  DE  79  B0 DE  00  BB  BF  7B  FD  1F  FF  FF\
FF  0F  """
Ireg_5=u"00  70  29  A5  94  52  4A  29  A5 94  52  4A  FF  FF  FF  FF"
Dreg_5=u""" \
0A  04  00  00  9C  75  4A  03  00 00  80  B3  4E  6D  00  00\
00  70  D6  A9  0D  00  00  00  9C 75  62  03  00  00  80  B3\
4E  6C  00  00  00  70  D6  A9  0D 00  00  00  9C  75  5A  03\
00  00  80  B3  4E  6C  00  00  00 70  D6  A9  0D  00  00  00\
9C  75  62  03  00  00  80  B3  4E 6C  00  00  00  70  D6  89\
0D  00  00  00  9C  75  62  03  00 00  80  B3  4E  6D  00  00\
00  70  D6  29  0D  00  00  00  9C 75  5A  03  00  00  80  B3\
4E  6C  00  00  00  70  D6  69  0D 00  00  00  9C  75  62  03\
00  00  80  B3  4E  6D  00  00  00 70  D6  69  0D  00  00  00\
9C  75  62  03  00  00  80  B3  4E 6C  00  00  00  70  D6  69\
0D  00  00  00  9C  75  62  03  00 00  80  B3  4E  6C  00  00\
00  70  D6  69  0D  00  00  00  9C 75  5A  03  00  00  80  B3\
4E  6C  00  00  00  70  D6  89  0D 00  00  00  9C  75  62  03\
00  00  80  B3  4E  6B  00  00  00 70  D6  69  0D  00  00  00\
9C  75  72  03  00  00  80  B3  4E 6B  00  00  00  70  D6  49\
0D  00  00  00  9C  75  62  03  00 00  80  B3  4E  6B  00  00\
00  70  D6  89  0D  00  00  00  9C 75  4A  03  00  00  80  B3\
4E  6B  00  00  00  70  D6  89  0D 00  00  00  9C  75  6A  03\
00  00  80  B3  4E  6B  00  00  00 70  D6  89  0D  00  00  00\
9C  75  5A  03  00  00  80  B3  4E 6C  00  00  00  70  D6  89\
0D  00  0F """
Ireg_6=u" 00  70  EF  BD  F7  DE  7B  EF  BD F7  DE  7B  C3  FF  FF  FF"
Dreg_6=u""" \
01  79  FF  FF  25  F0  01  00  00 00  00  00  C0  08  00  00\
00  00  00  00  80  06  E1  29  F8 20  03  00  00  C0  FF  3F\
04  02  41  00  59  00  32  00  00 C0  08  00  00  80  06  E1\
E9  01 """
Ireg_7=u"00  20  C3  FF  FF  FF"
Dreg_7=u""" \
01  69  25  F0  01  00  00  00  00 00  C0  08  00  00  00  00\
00  00  80  06  E1  29  F8  20  03 00  00  C0  FF  3F  06  02\
41  00  01  00  32  00  00  C0  08 00  00  80  06  E1  E9  01 """




def create_sim_config(buses,nodes,busAttrs=None,nodeAttrs=None,logFrames=False):
	filename='sim_config.xml'
	dcs_sim_config.write_prologue(filename)
	f=open(filename,'a')
	
	if busAttrs==None:
		busAttrs=''
	if nodeAttrs==None:
		nodeAttrs=''
	for bus in range(0,buses):
		bus_name='can'+str(bus)
		f.write ('<bus portName="'+bus_name+'" '+busAttrs+' ');
		if logFrames:
			f.write('framesDumpFileName="can'+str(bus)+'_frames.txt" ')
		f.write (' > \n');
		for node in nodes:
			f.write('<node type="elmb" id="'+str(node)+'" '+nodeAttrs+' segmentedSdoStoreFileName="sdo_can'+str(bus)+'_'+str(node)+'.txt" />\n')
		f.write('</bus>\n')
	f.close()	
	dcs_sim_config.write_epilogue(filename)
	

#def create_server_config(buses,nodes,nodeguard,sync,type=None):
#	filename='OPCUACANOpenServer.xml.source'
#	dcs_server_config.write_prologue(filename,type)
#	elmb_type='ELMB'
#	if type!=None:
#		if type=='mdt':
#			elmb_type='MDM_BARREL'
#	f=open(filename,'a')
#	
#	for bus in range(0,buses):
#		bus_name='can'+str(bus)
#		f.write ('<CANBUS speed="'+str(dcs_server_config.bitrate)+'" port="'+bus_name+'" type="'+dcs_server_config.connection_type+'" name="'+bus_name+'"  >\n');
#		f.write ('<NODEGUARD interval="'+str(nodeguard)+'"/>\n')
#		f.write ('<SYNC interval="'+str(sync)+'"/>\n')
#		for node in range(1,nodes+1):
#			f.write('<NODE type="'+elmb_type+'" name="elmb'+str(node)+'" nodeid="'+str(node)+'" segmentedSdoStoreFileName="sdo_can'+str(bus)+'_'+str(node)+'.txt" />\n')
#		f.write('</CANBUS>\n')
#	f.close()	
#	dcs_server_config.write_epilogue(filename)

def hexToBytes(str):
	res=[]
	ba = bytearray.fromhex(str.replace(","," "))
	for b in ba: res.append(Byte(b))
	return res

wc=None

def compare(test_bytes,r):
	errors=0
	for i in range(0,len(test_bytes)):
		if test_bytes[i].value != r.value[i]:
			print 'differs, got:'+str(test_bytes[i].value)+' second is'+str(r[i].value)
			errors=errors+1
	return errors

def toByteString(bytes):
	r=bytearray(b"")
	for b in bytes:
		r.append(b.value)
	bytestring=pyuaf.util.primitives.ByteString(r)
	return bytestring


asynchronousLock=None
currentWriteThreads=0
listRespondingNodes=[]

def asynchronousWriteCallback(r):
	global asynchronousLock
	global currentWriteThreads
	global listRespondingNodes
	try:
		try:
			print 'overallStatus='+str(r.overallStatus)
			if str(r.overallStatus)!='Good':
				with asynchronousLock:
					if r.requestHandle in listRespondingNodes:
						dcs_test_utils.log_detail(False,'asynchronousWriteCallback status not good:'+str(r.overallStatus),str(r))
		except Exception as e:
			print 'Exception in asynchronousWriteCallback: '+str(e)
		with asynchronousLock:
			print 'currentWriteThreads='+str(currentWriteThreads)+' will decrease'
			currentWriteThreads = currentWriteThreads-1
		print 'asynchronousWriteCallback finished'
	except Exception as e:
		print 'Exception in asynchronousWriteCallback: '+str(e)
	
	
	
def writeAllBusesAndNodes(register,data,reallyExistingNodes):
	global asynchronousLock
	global currentWriteThreads
	global listRespondingNodes
	for bus in range(0,paramNumBuses):
		for node in range(1, paramNumNodesPerBus+1):
			bytes=hexToBytes(data)
			addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus),str(node),register)
			addr_s = dcs_opc_ua_elmb.addr_to_str(addr)
			while True:
				myCurrentWriteThreads=0
				asynchronousLock.acquire()
				myCurrentWriteThreads = currentWriteThreads
				asynchronousLock.release()
				if myCurrentWriteThreads < paramMaxParallelWrites: break
			#	print 'Have to wait: current writes='+str(myCurrentWriteThreads)
				time.sleep(0.1)
			asynchronousLock.acquire()
			currentWriteThreads = currentWriteThreads+1
			asynchronousLock.release()
			print ('now writing: '+addr_s)
			res=dcs_opc_ua_elmb.client.beginWrite([addr],[ toByteString(bytes) ], 13, wc, None, asynchronousWriteCallback)
			if node in nodes:
				with asynchronousLock:
					listRespondingNodes.append(res.requestHandle)
			print ('after calling beginWrite result is '+str(res.overallStatus))
		


parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="64")
parser.add_option ("-b", "--buses", dest="buses", default="2")
parser.add_option ("--sync_interval", dest="sync_interval", default="0")
parser.add_option ("--ng_interval", dest="ng_interval", default="0")
parser.add_option ("--max_parallel_writes", dest="max_parallel_writes", default="16")
parser.add_option ("--segmented_sdo_delay", dest="segmented_sdo_delay", default="0.07")
parser.add_option ("--dead_elmb_ratio", dest="dead_elmb_ratio", default="0.0")
dcs_test_utils.parse_options(parser)
paramNumNodesPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=int(dcs_test_utils.options.buses)
paramMaxParallelWrites=int(dcs_test_utils.options.max_parallel_writes)
paramSegmentedSdoDelay=dcs_test_utils.options.segmented_sdo_delay
paramDeadElmbRatio=float(dcs_test_utils.options.dead_elmb_ratio)
syncInterval=int(dcs_test_utils.options.sync_interval)
ngInterval=int(dcs_test_utils.options.ng_interval)
# Start OPC UA Server 

os.system("rm -f sdo_can*.txt")
nodes=dcs_test_utils.create_range_with_probability(1, 1+paramNumNodesPerBus, 1.0-paramDeadElmbRatio)

dcs_server_config.create_basic_server_config(paramNumBuses,paramNumNodesPerBus,ngInterval,syncInterval,'mdt')
create_sim_config(paramNumBuses,nodes,'','segmentedSdoDelay="'+str(paramSegmentedSdoDelay)+'" ',dcs_test_utils.options.log_frames)
dcs_test_utils.start_server()
time.sleep(10)
try:
	dcs_opc_ua_elmb.connect()	
	dcs_sim_connection.open_connection()

	ws=pyuaf.client.settings.WriteSettings	()
	ws.callTimeoutSec=20
	wc=pyuaf.client.configs.WriteConfig()
	wc.serviceSettings=ws
#	init_dreg4()

	for bus in range(0,paramNumBuses):
		dcs_conn_log.call_method_bus('can'+str(bus),'Start')

	asynchronousLock = thread.allocate_lock()

	writeAllBusesAndNodes('Ireg_wr',Ireg_1,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_1,nodes)
	writeAllBusesAndNodes('Ireg_wr',Ireg_2,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_2,nodes)
	writeAllBusesAndNodes('Ireg_wr',Ireg_3,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_3,nodes)
	writeAllBusesAndNodes('Ireg_wr',Ireg_4,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_4,nodes)
	writeAllBusesAndNodes('Ireg_wr',Ireg_5,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_5,nodes)
	writeAllBusesAndNodes('Ireg_wr',Ireg_6,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_6,nodes)
	writeAllBusesAndNodes('Ireg_wr',Ireg_7,nodes)
	writeAllBusesAndNodes('Dreg_wr',Dreg_7,nodes)

	# here: wait until last write finishes
	
	while True:
		myCurrentWriteThreads=0
		asynchronousLock.acquire()
		myCurrentWriteThreads = currentWriteThreads
		asynchronousLock.release()
		if myCurrentWriteThreads == 0: break
		#print 'Have to wait: current writes='+str(myCurrentWriteThreads)
		time.sleep(0.1)

	time.sleep (2)
	
	dcs_sim_connection.all_write_segmented_sdo_to_file();

	# now verify
	

	for b in range(0, paramNumBuses):
		for n in nodes:
			res = os.system("diff good_transfer.txt sdo_can"+str(b)+"_"+str(n)+".txt")
			loc = 'can'+str(b)+' node'+str(n)
			if res==0:
				dcs_test_utils.log_detail(True, 'Transfer OK', loc)
			else:
				dcs_test_utils.log_detail(False, 'Transfer FAIL', loc)
			





	time.sleep(15)
	

finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

