#!/usr/bin/python

# PURPOSE:
# In PDO and SDO, for read and write, check all supported data types, with focus of generating proper framer, proper casting, etc

# HOW THIS IS DONE:
# The simulator keeps a special test buffer. This buffer is mapped into RPDO4 and TPDO4 and also for SDO object index=9999
# Additionally the same buffer can be inspected/altered by TextCommands
# So all possible conversions can be achieved

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
import dcs_sim_connection
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives import *
from optparse 		    import OptionParser


testCases = [
	["byte",   Byte(0),             "00000000"],
	['byte',   Byte(1),             "01000000"],
	['bool',   Boolean(True),       "01000000"],
	['bool',   Boolean(False),      "00000000"],
	['bit',    Boolean(True),       "01000000"],
	['bit',    Boolean(False),      "00000000"],	
	['Int16',  Int16(0),            "00000000"],
	['Int16',  Int16(1),            "01000000"],
	['Int16',  Int16(-1),           "FFFF0000"],
	['Int16',  Int16(-32768),       "00800000"],
	['UInt16', UInt16(0),           "00000000"],
	['UInt16', UInt16(1),           "01000000"],
	['UInt16', UInt16(32768),       "00800000"],
	['UInt16', UInt16(65535),       "FFFF0000"],
	['Int32',  Int32(0),            "00000000"],
	['Int32',  Int32(1),            "01000000"],
	['Int32',  Int32(256),          "00010000"],
	['Int32',  Int32(65536),        "00000100"],
	['Int32',  Int32(16777216),     "00000001"],
	['Int32',  Int32(2147483647),   "FFFFFF7F"],
	['Int32',  Int32(-1),           "FFFFFFFF"],
	['Int32',  Int32(-2147483648),  "00000080"],
	['UInt32', UInt32(0),           "00000000"],
	['UInt32', UInt32(1),           "01000000"],
	['UInt32', UInt32(256),         "00010000"],
	['UInt32', UInt32(65536),       "00000100"],
	['UInt32', UInt32(16777216),    "00000001"],
	['float',  Float(0.0),          "00000000"],
	['float',  Float(1),            "0000803F"],
	['float',  Float(-2),           "000000C0"],
	['float',  Float(0.25),         "0000803E"],
	['float',  Float(-0.25555),     "73D782BE"],
	]

# The restriction below: SDO may carry max 4 bytes in expedited mode
typesAllowedSdo=['byte','bool','bit','Int16','UInt16', 'Int32', 'UInt32', 'float']


def getStringAddr (comm, op, type):
	a=''
	if comm=="sdo":
		if type not in typesAllowedSdo:
			raise Exception('Logic error: SDO doesnt handle '+type)
		a='TestSdo.'+type;
	elif comm=='pdo':
		if op=='r':
			a='TPDO4.'+type
		elif op=='w':
			a='RPDO4.'+type
		else:
			raise Exception('Logic error: op='+op)
			
		pass
	else:
		raise Exception('Logic error: comm='+comm) 
	return 'can0.elmb1.'+a 

def refreshPDO4 ():
	dcs_opc_ua_elmb.client.write( [dcs_opc_ua_elmb.addr('can0.elmb1.TPDO4.RTRcommand')], [Boolean(True)] )
	time.sleep(1)

def testOp( comm, op, testCase ):	
	try:
		sa=getStringAddr(comm, op, testCase[0])
		print sa
		res=None
		if op=='w':
			res=dcs_opc_ua_elmb.client.write( [dcs_opc_ua_elmb.addr(sa)], [testCase[1]] )
			if not res.overallStatus:
				raise Exception("at write(): "+str(res))
			buff=dcs_sim_connection.get_test_buffer_9999('can0', 1)
			print 'Obtained from simulator: '+buff
			if buff != testCase[2]:
				dcs_test_utils.log_detail(False, 'While write of type='+testCase[0]+': in the ELMB simualator memory expected: '+testCase[2]+' but got '+buff, sa)
			else:
				dcs_test_utils.log_detail(True, 'While write of type='+testCase[0]+': OK', sa)
			
			
			# now verify what's into the buffer
		else:
			# inject the value into simulator
			dcs_sim_connection.set_test_buffer_9999('can0', 1, testCase[2])
			# for PDO we have to incvoke RTR to reread current value
			if comm=='pdo':
				refreshPDO4()
			res=dcs_opc_ua_elmb.client.read( [dcs_opc_ua_elmb.addr(sa)] )
			if not res.overallStatus:
				raise Exception("at read(): "+str(res))
			if res.targets[0].data is None:
				# means no value returned from the server
				dcs_test_utils.log_detail(False, 'While read of type='+testCase[0]+': value expected was '+str(testCase[1].value)+' but it is NULL', sa)
			else:
				v = res.targets[0].data.value
				if v  != testCase[1].value:
					dcs_test_utils.log_detail(False, 'While read of type='+testCase[0]+': value expected was '+str(testCase[1].value)+' but got '+str(v), sa)
				else:
					dcs_test_utils.log_detail(True, 'While read of type='+testCase[0]+': OK', sa)
			# TODO check res
	
			
		print res.overallStatus
	except Exception as e:
		print 'In testOp (comm='+comm+' op='+op+' testCase='+str(testCase)+') Exception: '+str(e)
		raise e
	return

def handleTestCase(testCase):
	for comm in ['pdo','sdo']:
		for op in ['r','w']:
			testOp(comm, op, testCase)
				


dcs_test_utils.parse_options()
# Start OPC UA Server 
dcs_test_utils.start_server()

try:
	dcs_opc_ua_elmb.connect()	

	time.sleep(5)

	for testCase in testCases:
		handleTestCase(testCase)
		

	time.sleep(5)


	



finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

