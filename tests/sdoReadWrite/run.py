#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
import dcs_server_config
import dcs_sim_config
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives import *
from optparse 		    import OptionParser
import	random


def createServerConfigFile(buses,nodes,nodeguard,sync):
	fname='OPCUACANOpenServer.xml.source'
	dcs_server_config.write_prologue(fname,'ELMB_local')
	f=open(fname,'a')
	for bus in range(0,buses):
		bus_name='can'+str(bus)
		f.write ('<CANBUS speed="'+str(dcs_server_config.bitrate)+'" port="'+bus_name+'" type="'+dcs_server_config.connection_type+'" name="'+bus_name+'"  >\n');
		f.write ('<NODEGUARD interval="'+str(nodeguard)+'"/>\n')
		f.write ('<SYNC interval="'+str(sync)+'"/>\n')
		for node in range(1,nodes+1):
			f.write('<NODE type="ELMB" name="elmb'+str(node)+'" nodeid="'+str(node)+'">\n')
			for ch in range(0,32):
				f.write('<ITEM name="item'+str(ch)+'" value="TPDO3.Value_'+str(ch)+'" /> \n')
			for ch in range(0,32):
				f.write('<ITEM name="item'+str(32+ch)+'" value="TPDO3.Value_'+str(2*ch)+'*TPDO3.Value_'+str(2*ch+1)+'" /> \n')
			f.write('</NODE> \n')
		f.write('</CANBUS>\n')
	f.close()	
	dcs_server_config.write_epilogue(fname)

def getMergedPortAValue():
	value = 0
	for i in range(0,8):
		addr = dcs_opc_ua_elmb.node_obj_addr('can0',1,'TPDO1.di_A_'+str(i))
		v = dcs_opc_ua_elmb.client.read([addr])
		if v.targets[0].data.value:
			value = value + (1 << i)
	return value

def doReadWrite():
	try:
		print 'About to write'
		wr_addr = dcs_opc_ua_elmb.node_obj_addr('can0',1,'do_write_sdo.do_A_write')
		val_w = random.randint (0,255)
		dcs_test_utils.log_detail(None, 'Writing (using do_write_sdo) to port A:'+str(val_w), dcs_opc_ua_elmb.addr_to_str(wr_addr))
		res=dcs_opc_ua_elmb.client.write ([wr_addr], [Byte(val_w)] )
		print 'After write: status='+str(res.overallStatus)
		time.sleep(1)
		rd_addr = dcs_opc_ua_elmb.node_obj_addr('can0',1,'di_read.di_A_read')
		r = dcs_opc_ua_elmb.client.read([rd_addr])
		print 'After read: status='+str(res.overallStatus)
		# and finally compare it with regular DI
		val_r=r.targets[0].data.value
		if val_w == val_r:
			dcs_test_utils.log_detail(True,'Value read by di_read (SDO) matches written value', dcs_opc_ua_elmb.addr_to_str(rd_addr))
		else:
			dcs_test_utils.log_detail(False, 'Value read by di_read(SDO) doesnt match. Read='+str(val_r), dcs_opc_ua_elmb.addr_to_str(rd_addr))
		dcs_conn_log.call_method_bus('can0', 'Synch')
		time.sleep(2)
		merged_v = getMergedPortAValue()
		print 'Merged value='+str(merged_v)
		if merged_v == val_w:
			dcs_test_utils.log_detail(True,'Value read by di_A (PDO) matches written value', '')
		else:
			dcs_test_utils.log_detail(False, 'Value read by di_A (PDO) doesnt match. Read='+str(merged_v), '')
		
	except Exception as e:
		print 'Exception:'+str(e)
	

parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="64")
parser.add_option ("--iterations", dest="iterations", default="2")
dcs_test_utils.parse_options(parser)
paramNumElmbsPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=1
paramNumIterations=int(dcs_test_utils.options.iterations)
# Start OPC UA Server 
createServerConfigFile (paramNumBuses,paramNumElmbsPerBus,0,0)
dcs_sim_config.create_basic_sim_config(paramNumBuses,paramNumElmbsPerBus,'','')

dcs_test_utils.start_server()
time.sleep(10)
try:
	dcs_opc_ua_elmb.connect()	
	dcs_conn_log.call_method_bus('can0', 'Start')
	for i in range(0,paramNumIterations):
		doReadWrite()

	#time.sleep(60)
finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

