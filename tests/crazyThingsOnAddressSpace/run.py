#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
from datetime import datetime
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.nodeclasses import *
from pyuaf.util.primitives import *
from pyuaf.util.opcuastatuscodes import *
from pyuaf.util.errors import  *

import random
import pyuaf.util.primitives
from dcs_test_utils import log_detail


# TODO in future:
# Add listShouldBeGood and listShouldFail and mark some colors accordingly
# This obviously can be done only for small portion of the test cases
# But the most important thing is that it won't crash

def resetServer():
	dcs_test_utils.start_server()
	time.sleep(5)

def checkReset(status):
	if status==OpcUa_BadConnectionClosed:
		dcs_test_utils.log_detail(False, 'Dead server detected, attempting restart', '')
		# rerun the server
		dcs_test_utils.start_server()
		time.sleep(5)
		return True
	else:
		return False

def checkResetExc(e):
	if isinstance(e,DisconnectionError) or isinstance(e,ConnectionError):
		dcs_test_utils.log_detail(False, 'Dead server detected, attempting restart', '')
		# rerun the server
		dcs_test_utils.start_server()
		time.sleep(5)
		return True
	else:
		return False

parser=dcs_test_utils.get_standard_parser()

dcs_test_utils.parse_options(parser)
options={}
if dcs_test_utils.options.valgrind:
	 options={'valgrind':True}
dcs_test_utils.start_server(options)

listWrites=[
		['can0.SynchInterval','UInt32','Good'],
		['can0.NodeGuardingInterval','UInt32','Good'],
		['can0.elmb1.NMT','Byte','Good'],
		['can0.elmb1.storeParams.save','UInt32','Good'],
		['can0.elmb1.diTransmit.diEventTimer','UInt32','Good'],
		['can0.elmb2.Ireg_wr','ByteString','Good']
		]

def getStringType(val):
	return str(type(val)).replace('>',' ').replace('<',' ').replace('pyuaf.util.primitives.',' ')

def testWrite(addr,val):
	print 'testWrite addr='+dcs_opc_ua_elmb.addr_to_str(addr)
	type_s=getStringType(val)
	attempt=1
	while attempt>0:
		try:
			res=dcs_opc_ua_elmb.client.write([addr],[val])
			dcs_test_utils.log_detail(None, 'Write: type='+type_s+' val='+str(val)+', status='+str(res.targets[0].status), dcs_opc_ua_elmb.addr_to_str(addr))
			break
		except Exception as e:
			print ('Exception '+str(e))

			dcs_test_utils.log_detail(False, 'testWrite(type='+type_s+') Exception of type'+getStringType(e)+':'+str(e),dcs_opc_ua_elmb.addr_to_str(addr))
			if checkResetExc(e):
				attempt=0
				continue
			else:
				break

def testRead(addr):
	print 'testRead addr='+dcs_opc_ua_elmb.addr_to_str(addr)
	try:
		res=dcs_opc_ua_elmb.client.read([addr])
		dcs_test_utils.log_detail(None, 'Read: status='+str(res.targets[0].status), dcs_opc_ua_elmb.addr_to_str(addr))
	except Exception as e:
		dcs_test_utils.log_detail(False,'Read failed: Exception:'+str(e),dcs_opc_ua_elmb.addr_to_str(addr))	
	

	
def testVariable(sa):
	print 'Test variable addr='+str(sa)
	addr=dcs_opc_ua_elmb.addr(sa)
	testWrite(addr,ByteString(bytearray(1000)))	
	testWrite(addr,Byte(2))
	testWrite(addr,Int16(30000))
	testWrite(addr,Int16(0))
	testWrite(addr,Int16(-15))
	testWrite(addr,UInt16(0))
	testWrite(addr,UInt16(1000))
	testWrite(addr,Int32(178654))
	testWrite(addr,Int32(-1786983))
	testWrite(addr,UInt32(1000))
	testWrite(addr,Int64(2999999999999))
	testWrite(addr,Int64(-2999999999999))
	testWrite(addr,UInt64(100000000000))
	testWrite(addr,Float(0.0))
	testWrite(addr,Float(10000.0))
	testWrite(addr,Float(-10000.0))
	testWrite(addr,Double(0.0))
	testWrite(addr,Double(10000.0))
	testWrite(addr,Double(-10000.0))
	testWrite(addr,String("str"))
	testWrite(addr,String("Rather quite long string ..... realllllllllllllllllly longg"))

	testRead(addr)
	
def testCallMethod(methodAddr, objAddr):
	res = dcs_opc_ua_elmb.client.call (dcs_opc_ua_elmb.addr(objAddr), dcs_opc_ua_elmb.addr(methodAddr))
	try:
		dcs_test_utils.log_detail(None, 'Call: method='+methodAddr+' obj='+objAddr+' status='+str(res.targets[0].status), '')
	except Exception as e:
		dcs_test_utils.log_detail(False,'Call failed: Exception:'+str(e),objAddr)

# no error-checked part
def recurse_internal(sa):
	result = dcs_opc_ua_elmb.client.browse([dcs_opc_ua_elmb.addr(sa)])
	for brt in result.targets:
		for ri in xrange(len(brt.references)):
			r = brt.references[ri]
			print 'Found: ' + str(r.displayName) + '  browseName=' + sa + '.' + str(r.browseName.name()) + ' nodeclass=' + str(r.nodeClass)			
			# 	import pdb; pdb.set_trace()
			# look only for variables
			if r.nodeClass == Variable:
				print 'Oooh, a variable !'
				testVariable(sa + '.' + r.browseName.name())
			if r.nodeClass == Object:
				recurse(sa + '.' + r.browseName.name())
			if r.nodeClass == Method:
				testCallMethod(sa + '.' + r.browseName.name(), sa)	
	
def recurse(sa):
	dcs_test_utils.log_detail(None, 'Recurse(browse)',sa)
	print 'RECURSE sa='+sa
	attempt=2
	while attempt>0:
#		try:
		recurse_internal(sa)
		return # no exception
#		except Exception as e:
#			dcs_test_utils.log_detail(False,'Browse failed: Exception:'+str(e),sa)
#			if checkResetExc(e):
#				attempt=attempt-1
#				continue
#			else:
#				return
	
				
	
	
try:
	time.sleep(5)
	dcs_opc_ua_elmb.connect()
	#our root node is bus's node
	bus='can0'
	recurse(bus)
# 	bus_node=dcs_opc_ua_elmb.bus_addr(bus)
# 	result = dcs_opc_ua_elmb.client.browse([bus_node])
# 	for brt in result.targets:
# 		for ri in xrange(len(brt.references)):
# 			r = brt.references[ri]
# 			print 'Found: '+str(r.displayName)+'  browseName='+str(r.browseName.name())+' nodeclass='+str(r.nodeClass)
# 			
# 			#	import pdb; pdb.set_trace()
# 			# look only for variables
# 			if r.nodeClass==Variable:
# 				print 'Oooh, a variable !'
# 				testVariable(bus+'.'+r.browseName.name())
# 			if r.nodeClass==Object:
# 				recurse
# 			
				
except  Exception as e:
	print str(e)



finally:
	
	dcs_test_utils.kill_server()


dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()
