#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
import dcs_sim_connection
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from optparse 		    import OptionParser
from pyuaf.util.primitives import *

nodeRtrYes = 1
nodeRtrInit = 2

def checkInterval (desired_interval, which):
	if which=='Synch':
		sync_interval_addr = dcs_opc_ua_elmb.bus_obj_addr('can0', 'SynchInterval');
	else:
		sync_interval_addr = dcs_opc_ua_elmb.bus_obj_addr('can0', 'NodeGuardingInterval');
	dcs_opc_ua_elmb.client.write([sync_interval_addr], [UInt32(0)])
	time.sleep(1)
	if which=='Synch':
		number_before = dcs_sim_connection.get_num_received_sync('can0', nodeRtrYes)
	else:
		number_before = dcs_sim_connection.get_num_received_nodeguard('can0', nodeRtrYes)
	dcs_opc_ua_elmb.client.write([sync_interval_addr], [UInt32(desired_interval)])
	measure_time=60
	time.sleep(measure_time)
	if which=="Synch":
		number_after = dcs_sim_connection.get_num_received_sync('can0', nodeRtrYes)
	else:
		number_after = dcs_sim_connection.get_num_received_nodeguard('can0', nodeRtrYes)
	if number_after==number_before: #no requests received
		dcs_test_utils.log_detail(False, which+'Fail: no requests received in the interval of question', '')
	else:
		obtained_sync_interval = 1000*measure_time/(number_after-number_before)
		print 'sync interval: desired='+str(desired_interval)+' obtained='+str(obtained_sync_interval)
	if abs(desired_interval-obtained_sync_interval)/desired_interval < 0.05:
		dcs_test_utils.log_detail(True, which+'Interval test passed for interval='+str(desired_interval),'')
	else:
		dcs_test_utils.log_detail(False, which+'Interval failed, desired interval='+str(desired_interval)+' obtained='+str(obtained_sync_interval), '')
	dcs_opc_ua_elmb.client.write([sync_interval_addr], [UInt32(0)])

dcs_test_utils.parse_options()
# Start OPC UA Server 
dcs_test_utils.start_server()

try:
	dcs_opc_ua_elmb.connect()	
	dcs_sim_connection.open_connection()
#	dcs_conn_log.call_method_bus('can0','Start')
	num_rtr=0
	num_rtr_new=0
	

# 	addr_yes = dcs_opc_ua_elmb.node_obj_addr ('can0',nodeVeryDelayed, 'serialNumber')
# 	addr4_s = dcs_opc_ua_elmb.addr_to_str(addr4)
	# Test1 . "rtr=yes" node shall have its rtr counter at 0
	num_rtr = dcs_sim_connection.get_num_received_rtr_tpdo3('can0', nodeRtrYes)
	if num_rtr==0:
		dcs_test_utils.log_detail(True, 'rtr=yes, initial #rtrs=0', 'can0 elmb'+str(nodeRtrYes))
	else:
		dcs_test_utils.log_detail(False, 'rtr=yes, initial #rtrs!=0 : '+str(num_rtr), 'can0 elmb'+str(nodeRtrYes))

	num_rtr = dcs_sim_connection.get_num_received_rtr_tpdo3('can0', nodeRtrInit)
	if num_rtr==1:
		dcs_test_utils.log_detail(True, 'rtr=init, initial #rtrs=1', 'can0 elmb'+str(nodeRtrInit))
	else:
		dcs_test_utils.log_detail(False, 'rtr=init, initial #rtrs!=1 : '+str(num_rtr), 'can0 elmb'+str(nodeRtrInit))

	num_rtr = dcs_sim_connection.get_num_received_rtr_tpdo3('can0', nodeRtrYes)
	num_rtr_new = num_rtr
	# now call a RTR Commands on elmb1
	addr = dcs_opc_ua_elmb.addr('can0.elmb'+str(nodeRtrYes)+'.TPDO3.RTRcommand')
	addr_s = dcs_opc_ua_elmb.addr_to_str(addr)
	val=Int16(1)
	res = dcs_opc_ua_elmb.client.write([addr], [val]);
	if str(res.overallStatus)!='Good':
		dcs_test_utils.log_detail(False, 'Writing to TPDO3.RTRcommand, result was='+str(res.targets[0].status),addr_s)
	else:
		# result was fine, so see if we really received that msg after some time
		time.sleep(1)
		num_rtr_new = dcs_sim_connection.get_num_received_rtr_tpdo3('can0', nodeRtrYes)
		if num_rtr_new-num_rtr==1:
			dcs_test_utils.log_detail(True, 'RTRcommand passed the test', '')
		else:
			dcs_test_utils.log_detail(False, 'RTRcommand failed the test, expected 2, got:'+str(num_rtr),'')
	num_rtr = num_rtr_new
	# now call a RTR Method
	
	res = dcs_opc_ua_elmb.tpdo3_rtr('can0', nodeRtrYes)
	if str(res.overallStatus) != 'Good':
		dcs_test_utils.log_detail(False, 'RTR method call failed:'+str(res.targets[0].status),'')
	else:
		print "the result of TPDO3 RTR call: "+str(res)
		time.sleep(1)
		num_rtr_new = dcs_sim_connection.get_num_received_rtr_tpdo3('can0', nodeRtrYes)
		if num_rtr_new-num_rtr==1:
			dcs_test_utils.log_detail(True, 'RTR method passed test', '')
		else:
			dcs_test_utils.log_detail(False, 'RTR method didnt pass the test, RTR counter diff='+str(num_rtr_new-num_rtr),'')

	# Now the part with checking sync interval and ng interval
	# Simply pick some interval for sync and then count number of syncs in e.g. 30 seconds

#	checkInterval(200, "Synch");
#	checkInterval(5000, "Synch");
#	checkInterval(200, "NodeGuading");
#	checkInterval(5000, "NodeGuarding");



finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

