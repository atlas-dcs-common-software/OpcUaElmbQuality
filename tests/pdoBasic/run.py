#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_server_config
import dcs_sim_config
import dcs_conn_log
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings

#TEST_PARAMS
paramNumBuses=1
paramNumElmbsPerBus=16
paramNumAnalogChannelsPerElmb=64
paramNumIterations=2
paramDontLogGoodValues=False

def testValues(iteration,sync_times):
	expected=iteration
	all=paramNumBuses*paramNumElmbsPerBus*paramNumAnalogChannelsPerElmb
	ctr=0
	for bus in range(0,paramNumBuses):
		for elmb in range(1,paramNumElmbsPerBus+1):
			for channel in range(0,paramNumAnalogChannelsPerElmb):
				print '\rProgress: '+str(ctr)+' / '+str(all)+' ...'
				ctr=ctr+1
				try:
					addr = dcs_opc_ua_elmb.pdo3_addr('can'+str(bus),elmb,channel)
					addr_s = dcs_opc_ua_elmb.addr_to_str(addr)
					result = dcs_opc_ua_elmb.client.read([ addr ])
				#	print result.overallStatus
					if (str(result.overallStatus)!='Good'):
						dcs_test_utils.log_detail (False,'Status is not good',addr_s)
					else:
						result_v=result.targets[0].data.value
				#		print("The counter value is %d" %result_v)
						if result_v!=expected:
							dcs_test_utils.log_detail (False,'Read value='+str(result_v)+' expected='+str(expected),addr_s)
						else:
							if not paramDontLogGoodValues:
								dcs_test_utils.log_detail (True,'',addr_s) 
					if sync_times != None:
						time_diff = result.targets[0].sourceTimestamp.toTime_t() - sync_times[bus]
						if time_diff>=0 and time_diff<120:
							if not paramDontLogGoodValues:
								dcs_test_utils.log_detail(True,'Source timestamp fine,time_diff='+str(time_diff),addr_s)
						else:
							dcs_test_utils.log_detail(False,'Source timestamp fail,time_diff='+str(time_diff),addr_s)
						time_diff = result.targets[0].serverTimestamp.toTime_t() - sync_times[bus]
						if time_diff>=0 and time_diff<120:
							if not paramDontLogGoodValues:
								dcs_test_utils.log_detail(True,'Server timestamp fine,time_diff='+str(time_diff),addr_s)
						else:
							dcs_test_utils.log_detail(False,'Server timestamp fail,time_diff='+str(time_diff),addr_s)
				except Exception as e:
					dcs_test_utils.log_detail (False,'Exception '+str(e),'')
					print sys.exc_info()[1].message


parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="64")
parser.add_option ("-b", "--buses", dest="buses", default="2")
parser.add_option ("--iterations", dest="iterations", default="2")
parser.add_option ("--dont_log_good_values", dest="dont_log_good_values", action="store_true") 
dcs_test_utils.parse_options(parser)
paramNumElmbsPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=int(dcs_test_utils.options.buses)
paramNumIterations=int(dcs_test_utils.options.iterations)
paramDontLogGoodValues=dcs_test_utils.options.dont_log_good_values
paramLogFrames=dcs_test_utils.options.log_frames
# Start OPC UA Server 
dcs_server_config.create_basic_server_config(paramNumBuses,paramNumElmbsPerBus,0,0)
dcs_sim_config.create_basic_sim_config(paramNumBuses,paramNumElmbsPerBus,'','',paramLogFrames)
run_options={}
if dcs_test_utils.options.valgrind:
	run_options={'valgrind':True}
dcs_test_utils.start_server(run_options)

try:
	time.sleep(10)
	dcs_opc_ua_elmb.connect()	
	for bus in range(0,paramNumBuses):
		dcs_conn_log.call_method_bus('can'+str(bus),'Start')
	for iter in range(1,paramNumIterations+1):
		dcs_test_utils.log_detail(None,'Beginning iteration '+str(iter),'')
		sync_times=[]
		# call sync on all buses and remember when it was issued
		for bus in range(0,paramNumBuses):
			dcs_conn_log.call_method_bus('can'+str(bus),'Synch')
			sync_times.append(pyuaf.util.DateTime.now().toTime_t())
			
		# give some time after sync -- after one sync on every bus would send 64*paramNumElmbsPerBus TPDO3 (every about 100bits length) 
		# so we should divide that by bus speed and multiply by some common factor eg.2
		timeFromBusSpeed=2*64.0*paramNumElmbsPerBus*100.0 / 125000
		# common factor * number channels / conversion Rate
		timeFromConversionRate=2*64/100.0
		time.sleep(max(timeFromBusSpeed,timeFromConversionRate))

		testValues(iter,sync_times)
	dcs_test_utils.log_detail(None,'Now trying per-node Synch','')


### I forgot sync doesn't make sense per node .... but maybe RTR would be fine?
#
#	# now call sync individually on every node
#	for bus in range (0,paramNumBuses):
#		for node in range(0,paramNumElmbsPerBus):
#			dcs_conn_log.call_method_node('can'+str(bus),node,'Synch')
#	time.sleep(5)
#	testValues(paramNumIterations+1,None)
	
	
finally:
	dcs_test_utils.kill_server()


dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

