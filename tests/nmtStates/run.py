#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_sim_connection
import dcs_sim_config
import dcs_conn_log
import dcs_server_config
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
# DESCRIPTION
# State changes to issue
# 1. Reset (goes to Preop after few seconds)
# 2. Start (goes to Operational right away)
# 3. Stop (goes to Stopped right away)
# 4. Preop (goes to Preoperational)
# 5. Reset (goes to Preoperational 

# On what these state changes are issues?
# 1. On whole bus (all nodes are checked to change to proper states)
# 2. Individual node on a bus (only one node is required to change state, others ARE REQUIRED NOT TO CHANGE STATE

# What is checked?
# 1. Nodes' state (using sim connection)
# 2. Nodes' bootup time (using OPC UA connection)

# TEST_PARAMS


# Verification
nodes=4
def verifyNode(bus,node,expectedState):
	try:
		state=dcs_sim_connection.get_node_nmt_state(bus,node)
		if (state==expectedState):
			dcs_test_utils.log_detail(True,'OK(sim) '+state,bus+' '+str(node))
		else:
			dcs_test_utils.log_detail(False,'(sim)Read='+state+' Expected='+expectedState,bus+' '+str(node))
	except Exception as e:
		dcs_test_utils.log_detail(False,'(sim)Read EXCEPTION '+str(e),bus+' '+str(node));
	try:
		state=dcs_opc_ua_elmb.node_nmt_state(bus,node)
		if (state==expectedState):
			dcs_test_utils.log_detail(True,'OK(opcua) '+state,bus+' '+str(node))
		else:
			dcs_test_utils.log_detail(False,'(opcua)Read='+state+' Expected='+expectedState,bus+' '+str(node))
	except Exception as e:
		dcs_test_utils.log_detail(False,'(opcua)Read EXCEPTION '+str(e),bus+' '+str(node));

def verifyAllNodesButOne(bus,expectedState,excludedNode):
	for node in range(1,nodes+1):
		if node==excludedNode: continue
		verifyNode(bus,node,expectedState)
		
def verifyBootupTime(bus,node,reset_time):
	addr=dcs_opc_ua_elmb.node_obj_addr(bus,node,'Bootup')
	(status,result)=dcs_conn_log.read_expect_good(addr)
	if not status: return # error already logged
	time_diff = result.targets[0].sourceTimestamp.toTime_t() - reset_time
	# bootup message normally comes 2-3 seconds after reset
	if time_diff>=0 and time_diff<20:
		dcs_test_utils.log_detail (True,'(Source)Bootup time is fine, time_diff='+str(time_diff), bus+' '+str(node))
	else:
		dcs_test_utils.log_detail (False,'(Source)Bootup time is strange, time_diff='+str(time_diff), bus+' '+str(node))
	time_diff = result.targets[0].serverTimestamp.toTime_t() - reset_time
	if time_diff>=0 and time_diff<20:
		dcs_test_utils.log_detail (True,'(Server)Bootup time is fine, time_diff='+str(time_diff), bus+' '+str(node))
	else:
		dcs_test_utils.log_detail (False,'(Server)Bootup time is strange, time_diff='+str(time_diff), bus+' '+str(node))
		
	
def callMethodManyBuses(numBuses,method):
	for i in range(0,numBuses):
		dcs_conn_log.call_method_bus('can'+str(i),method)

def callMethodNodeManyBuses (numBuses,node,method):
	for i in range(0,numBuses):
		dcs_conn_log.call_method_node('can'+str(i),node,method)

def verifyAllNodesButOneManyBuses(numBuses,expected,excludedNode):
	for i in range(0,numBuses):
		verifyAllNodesButOne ('can'+str(i),expected,excludedNode)

def verifyNodeManyBuses(numBuses,node,expectedState):
	for i in range(0,numBuses):
		verifyNode ('can'+str(i),node,expectedState)


def verifyBootupTimeManyBuses (numBuses,node,reset_time):
	for i in range(0,numBuses):
		verifyBootupTime ('can'+str(i),node,reset_time)

# problem --> in the sim we use can0
# here we use just 0

time.sleep(5)
parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="64")
parser.add_option ("-b", "--buses", dest="buses", default="2")
dcs_test_utils.parse_options(parser)
nodes=int(dcs_test_utils.options.nodes)
buses=int(dcs_test_utils.options.buses)
dcs_server_config.create_basic_server_config(buses,nodes,1000,0)
dcs_sim_config.create_basic_sim_config(buses,nodes)
# Start OPC UA Server 
dcs_test_utils.start_server()
time.sleep(5)
dcs_sim_connection.open_connection()
try:
	dcs_opc_ua_elmb.connect()
	dcs_test_utils.log_detail (None,'Now:Bus-level methods','')
	callMethodManyBuses(buses,'Reset')
	time.sleep (5	)
	verifyAllNodesButOneManyBuses(buses,'preoperational',None)

	callMethodManyBuses(buses,'Start')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'operational',None)

	callMethodManyBuses(buses,'PreOperation')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'preoperational',None)

	callMethodManyBuses(buses,'Stop')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'stopped',None)

	reset_time = pyuaf.util.DateTime.now().toTime_t()
	callMethodManyBuses(buses,'Reset')
	time.sleep (10)
	# after reset: node should be put back to the previous state that it had before reset
	# and use bootup time to determine if reset really happened
	verifyAllNodesButOneManyBuses(buses,'stopped',None)
	for n in range(1,1+nodes):
		verifyBootupTimeManyBuses (buses,n,reset_time)

	callMethodManyBuses(buses,'Stop')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'stopped',None)

	# now moreless the same but on changes on node level
	
	dcs_test_utils.log_detail (None,'Now:Node-level methods','')
	testNode=2

	callMethodNodeManyBuses(buses,testNode,'Start')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'stopped',testNode)
	verifyNodeManyBuses(buses,testNode,'operational') 

	callMethodNodeManyBuses(buses,testNode,'PreOperation')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'stopped',testNode)
	verifyNodeManyBuses(buses,testNode,'preoperational') 

	callMethodNodeManyBuses(buses,testNode,'Stop')
	time.sleep (1)
	verifyAllNodesButOneManyBuses(buses,'stopped',testNode)
	verifyNodeManyBuses(buses,testNode,'stopped') 

	callMethodNodeManyBuses(buses,testNode,'Start')
	time.sleep (2)
	verifyAllNodesButOneManyBuses(buses,'stopped',testNode)
	verifyNodeManyBuses(buses,testNode,'operational') 

	reset_time = pyuaf.util.DateTime.now().toTime_t()
	callMethodNodeManyBuses(buses,testNode,'Reset')
	time.sleep (10)
	verifyAllNodesButOneManyBuses(buses,'stopped',testNode) # everything except testNode
	# verify boot-up time
	verifyBootupTimeManyBuses (buses,testNode,reset_time)
	#import pdb; pdb.set_trace()
	verifyNodeManyBuses(buses,testNode,'operational') 

finally:
	
	dcs_test_utils.kill_server()


dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()
