#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_server_config
import dcs_sim_config
import dcs_conn_log
import random
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives import *
from pyuaf.util.statuscodes import *


# once it shall move to time-based operation
paramIterations=2000
paramSleep=0.005
paramMaxParallelOps=1
paramRunningTime=60
paramLogging=False
currentWriteThreads=0

paramStepsAtOnce=0
globalLock=None
quitDesired=False
callbackInvocations=0

def createServerConfigFile(buses,nodes,nodeguard,sync):
	fname='OPCUACANOpenServer.xml.source'
	dcs_server_config.write_prologue(fname,'ELMB_local')
	f=open(fname,'a')
	for bus in range(0,buses):
		bus_name='can'+str(bus)
		f.write ('<CANBUS speed="'+str(dcs_server_config.bitrate)+'" port="'+bus_name+'" type="'+dcs_server_config.connection_type+'" name="'+bus_name+'"  >\n');
		f.write ('<NODEGUARD interval="'+str(nodeguard)+'"/>\n')
		f.write ('<SYNC interval="'+str(sync)+'"/>\n')
		for node in range(1,nodes+1):
			f.write('<NODE type="ELMB" name="elmb'+str(node)+'" nodeid="'+str(node)+'">\n')
			for ch in range(0,32):
				f.write('<ITEM name="item'+str(ch)+'" value="TPDO3.Value_'+str(ch)+'" /> \n')
			for ch in range(0,32):
				f.write('<ITEM name="item'+str(32+ch)+'" value="TPDO3.Value_'+str(2*ch)+'*TPDO3.Value_'+str(2*ch+1)+'" /> \n')
			f.write('</NODE> \n')
		f.write('</CANBUS>\n')
	f.close()	
	dcs_server_config.write_epilogue(fname)


op_READ=1
op_WRITE=2	
	
def writeCallback(r):
	print 'writeCallback r='+str(r)	
	
def genericCallbackNoteNotGood(r):
	global globalLock
	global currentWriteThreads
	global callbackInvocations
	with globalLock:
		callbackInvocations=callbackInvocations+1
		try:
			if r.targets[0].status != Good:
				dcs_test_utils.log_detail(False, 'Status not good: '+str(r.targets[0].status), '')
		except Exception as e:
			dcs_test_utils.log_detail(False, 'Exception: '+str(e))
		finally:
			currentWriteThreads=currentWriteThreads-1

		
	
def thread_reading_per_node (list_obj,op,val=None):
	global globalLock
	global currentWriteThreads
	currentNode=1
	currentObject=0
	currentBus=0
	addr=None
	while True:
		try:
			with globalLock:
				_quitDesired = quitDesired
			if _quitDesired:
				break
			obj = list_obj[currentObject]
			node = currentNode
			bus=currentBus
			currentObject = currentObject+1
			if len(list_obj) == currentObject:
				currentObject=0
				currentNode = currentNode+1
				if currentNode > paramNumElmbsPerBus:
					currentNode=1
					currentBus=currentBus+1
					if currentBus == paramNumBuses:
						currentBus=0
			# enough information to act.
			addr = dcs_opc_ua_elmb.node_obj_addr('can'+str(bus), node, obj)	
			res=None
			# make sure than no more operations than allowed is there
			
			waitCtr=0
			while True:
				with globalLock:
					if currentWriteThreads < paramMaxParallelOps: 
						currentWriteThreads = currentWriteThreads+1
						break
			#	print 'Have to wait: current writes='+str(myCurrentWriteThreads)
				waitCtr = waitCtr+1
				if waitCtr > 50:
					print 'Already waiting 5 seconds for going on ....'
					waitCtr=0
					with globalLock:
						dcs_test_utils.log_detail(False,'Waiting for long time...',dcs_opc_ua_elmb.addr_to_str(addr))
				time.sleep(0.1)
			
			if op==op_READ:
				res=dcs_opc_ua_elmb.client.beginRead([addr],13,None,None,genericCallbackNoteNotGood)
			elif op==op_WRITE:
				print 'writing address: '+dcs_opc_ua_elmb.addr_to_str(addr)
				res=dcs_opc_ua_elmb.client.beginWrite([addr],[val],13,None,None,genericCallbackNoteNotGood)
				
			
			time.sleep(paramSleep)
		except Exception as e:
			print 'Exception: '+str(e)
			with globalLock:
				dcs_test_utils.log_detail(False, 'thread reading per node Exception: '+str(e), dcs_opc_ua_elmb.addr_to_str(addr) )
		
		
		
		
time.sleep(5)	
globalLock = thread.allocate_lock()
parser=dcs_test_utils.get_standard_parser()
parser.add_option ("-n", "--nodes", dest="nodes", default="1")
parser.add_option ("-b", "--buses", dest="buses", default="1")
parser.add_option ("--delay", dest="delay", default="0.01")
parser.add_option ("--max_parallel_ops", dest="max_parallel_ops", default="3")
parser.add_option ("--running_time", dest="running_time", default="60")
parser.add_option ("--logging", dest="logging", default=0)

parser.add_option ("--do_write_sdo", dest="do_write_sdo", action="store_true")
parser.add_option ("--do_read_sdo", dest="do_read_sdo", action="store_true")
parser.add_option ("--do_write_pdo", dest="do_write_pdo", action="store_true")
parser.add_option ("--do_read_pdo", dest="do_read_pdo", action="store_true")
parser.add_option ("--do_write_segmented_sdo", dest="do_write_segmented_sdo", action="store_true")
parser.add_option ("--do_read_segmented_sdo", dest="do_read_segmented_sdo", action="store_true")
parser.add_option ("--do_read_calc_items", dest="do_read_calc_items", action="store_true")

dcs_test_utils.parse_options(parser)
paramNumElmbsPerBus=int(dcs_test_utils.options.nodes)
paramNumBuses=int(dcs_test_utils.options.buses)
paramSleep=float(dcs_test_utils.options.delay)
paramMaxParallelOps=int(dcs_test_utils.options.max_parallel_ops)
paramRunningTime=int(dcs_test_utils.options.running_time)
if dcs_test_utils.options.logging:
	paramLogging = True

# Start OPC UA Server 

# Create the config files
createServerConfigFile(paramNumBuses,paramNumElmbsPerBus, 60000, 60000)

dcs_sim_config.create_basic_sim_config(paramNumBuses,paramNumElmbsPerBus,'','',True)
dcs_test_utils.start_server()

try:
	time.sleep(5)
	dcs_opc_ua_elmb.connect()	
	time.sleep(5)
	if paramLogging:
		ret=dcs_opc_ua_elmb.client.write([Address(NodeId('TraceLevel',2), dcs_opc_ua_elmb.server_uri)], [UInt32(255)]) 
		print ret
		if not ret.overallStatus.isGood():
			raise Exception (str(ret))
		print 'Logging enabled'
	for bus in range(0,paramNumBuses):
		dcs_conn_log.call_method_bus('can'+str(bus),'Start')


	if dcs_test_utils.options.do_read_pdo:
		list_read_pdo=[]
		for i in range(0,10):
			list_read_pdo.append('TPDO3.Value_'+str(i))
		thread.start_new_thread(thread_reading_per_node,(list_read_pdo,op_READ,))
	
	if dcs_test_utils.options.do_read_calc_items:
		list_read_item=[]
		for i in range(0,10):
			list_read_item.append('item'+str(i))
		thread.start_new_thread(thread_reading_per_node,(list_read_item,op_READ,))
	
	if dcs_test_utils.options.do_read_segmented_sdo:
		list_read_segmented_sdo=['Ireg_wr']
		thread.start_new_thread(thread_reading_per_node,(list_read_segmented_sdo,op_READ,))
	
	if dcs_test_utils.options.do_write_segmented_sdo:
		list_write_segmented_sdo=['Ireg_wr']
		thread.start_new_thread(thread_reading_per_node,(list_write_segmented_sdo,op_WRITE,ByteString(bytearray(20)),))

	if dcs_test_utils.options.do_write_pdo:
		list_write_pdo=['RPDO1.do_write']
		thread.start_new_thread(thread_reading_per_node,(list_write_pdo,op_WRITE,UInt16(4234),) )
	
	if dcs_test_utils.options.do_write_sdo:
		list_write_sdo=['doInitHigh']
		thread.start_new_thread(thread_reading_per_node,(list_write_sdo,op_WRITE,Byte(213),) )
	
	if dcs_test_utils.options.do_read_sdo:
		list_read_sdo=['hwVersion']
		thread.start_new_thread(thread_reading_per_node,(list_read_sdo,op_READ,) )
	
	time.sleep(paramRunningTime)
	with globalLock:
		quitDesired=True
	time.sleep(2)
	
	if callbackInvocations>0:
		dcs_test_utils.log_comment('# callback invocations:'+str(callbackInvocations))
	else:
		dcs_test_utils.log_detail(False,'#callback invocations is zero','')
	
	
finally:
	dcs_test_utils.kill_server()


dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

