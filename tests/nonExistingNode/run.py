#!/usr/bin/python

import thread
import time
import os
import pyuaf
import sys
import copy
import dcs_test_utils
import dcs_opc_ua_elmb
import dcs_conn_log
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from optparse 		    import OptionParser
from pyuaf.util.primitives  import Byte 

dcs_test_utils.parse_options()
# Start OPC UA Server 
dcs_test_utils.start_server()

try:
	dcs_opc_ua_elmb.connect()	
	# Note: serialNumber SDO has a big timeout, hwVersion SDO has a small timeout
	addr1 = dcs_opc_ua_elmb.node_obj_addr ('can0',666,'nonExistingSerialNumber')
	addr1_s = dcs_opc_ua_elmb.addr_to_str(addr1)
	try:
		res = dcs_opc_ua_elmb.client.read ([addr1])
		dcs_test_utils.log_detail (None,'read status='+str(res.targets[0].status),'addr_1')
		res = dcs_opc_ua_elmb.client.write ([addr1], [Byte(100)])
		dcs_test_utils.log_detail (None,'read status='+str(res.targets[0].status),'addr_1')
		
	except Exception as e:
		dcs_test_utils.log_detail(False,'Unexpected exception: '+str(e),'')



finally:
	dcs_test_utils.kill_server()
dcs_test_utils.create_detailed_html  ()
dcs_test_utils.create_summary_pickle ()

