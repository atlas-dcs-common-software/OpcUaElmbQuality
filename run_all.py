#!/usr/bin/python
import markup
import os
import pickle
import time
from optparse 		    import OptionParser
from datetime import datetime
page=None


parser = OptionParser()
parser.add_option ("--run_sock", dest="run_sock", action="store_true")
parser.add_option ("--run_simulated", dest="run_simulated", action="store_true")
parser.add_option ("--limit_detailed_reports", action="store_true")
parser.add_option ("--valgrind", dest="valgrind", action="store_true")
(options,args) = parser.parse_args()
run_simulated=False
run_real_can=False
run_valgrind=False
if options.run_simulated:
	run_simulated=True
if options.run_sock:
	run_real_can=True
if options.valgrind:
	print "Setting valgrind=True"
	run_valgrind=True


run_simulated=options.run_simulated
run_real_can=options.run_sock

if not run_simulated and not run_real_can:
	print 'Warning: neither --run_sock nor --run_simulated given!'

output_path='output'

def adapt_systec_timeout(tim):
	path='/etc/modprobe.d/systec_can.conf'
	text='options systec_can tx_timeout_ms='+str(tim)
	f=open(path,'w')
	f.write(text)
	f.close()
	os.system('/sbin/rmmod systec_can')
	time.sleep(10)
	r=os.system('/sbin/modprobe systec_can')
	print 'modprobe result:'+str(r)
	time.sleep(10)
	os.system('/opt/systec_can/systec_ports_up.py 8 15')
	time.sleep(6)

	

def run_test(name,output='output',params=' ',extra_info=' '):
	if run_valgrind:
		params='--valgrind '+params

	print '**************************************************************************************'
	print 'Now doing: '+name+' params='+params+' extra_info='+extra_info
	print '**************************************************************************************'
	output=str(output)
	page.tr()
	page.td(name)
	page.td.close()

	page.td(params)
	page.td.close()

	if options.limit_detailed_reports:
		params = params + ' --limit_detailed_reports '

	page.td(extra_info)
	page.td.close()

	# measure time
	t1=datetime.now()	
	ret=os.system('cd tests/'+name+'; python run.py --output='+output+' '+params)
	t2=datetime.now()
	tdelta=t2-t1
	if (ret==0):
		page.td(ret, bgcolor='SpringGreen')
	else:
		page.td(ret, bgcolor='Red')

	try:
		f=open(output_path+'/'+output+'.pickle')
		summary=pickle.load(f)

		page.td(summary['num_all'])
		page.td.close()
		perc_bad_s="%10.2f"%(100.0*summary['num_bad']/summary['num_all'])
		if summary['num_bad']>0:
			page.td(perc_bad_s,bgcolor='Red')
		else:
			page.td(perc_bad_s,bgcolor='SpringGreen')
		page.td.close()

		if summary['server_crashed']:
			page.td(summary['server_crashed'],bgcolor='Violet')
		else:
			page.td(summary['server_crashed'],bgcolor='')
		page.td.close()
	except:
		page.td('no pickle!')
		page.td.close()
		page.td('no pickle!')
		page.td.close()
		page.td('no pickle!')
		page.td.close()
	
	page.td(str(tdelta))
	page.td.close()

	page.td()
	page.a('Details', href=output+'.html')
	page.a.close()
	page.tr.close()

def run_test_multiple_interfaces(name,output='output',params=' '):
	output=name+'_'+str(output)
	# the default one is fully simulated
	if run_simulated:
		run_test(name,output+'_sim',params)
	if run_real_can:
		run_test(name,output+'_sock',params+' --can_interface=sock --sim_bus_shift=7 ','')

page=markup.page()
page.init(title='Summary')
page.table(border='1')
page.tr()
page.td('name')
page.td.close()

page.td('opts')
page.td.close()

page.td('extrainfo')
page.td.close()

page.td('return val')
page.td.close()

page.td('#Tests')
page.td.close()

page.td('%Failed')
page.td.close()

page.td('Server crashed?')
page.td.close()

page.td('Time taken')
page.td.close()

page.td('links')
page.td.close()
page.tr.close()

# run_test_multiple_interfaces ('nmtStates',0, '--buses 1 --nodes 3')
# run_test_multiple_interfaces ('nmtStates',1, '--buses 2 --nodes 20')
# run_test_multiple_interfaces ('nmtStates',2, '--buses 7 --nodes 64')
 
# run_test_multiple_interfaces ('pdoBasic','0', '--buses 1 --nodes 3  --iterations 20')
# run_test_multiple_interfaces ('pdoBasic','1', '--buses 2 --nodes 20 --iterations 10')
# run_test_multiple_interfaces ('pdoBasic','2', '--buses 7 --nodes 64 --iterations 5')

run_test_multiple_interfaces ('pdoWithStatus','0', '--buses 1 --nodes 3  --iterations 1')
# run_test_multiple_interfaces ('pdoWithStatus','1', '--buses 2 --nodes 20 --iterations 10')
# run_test_multiple_interfaces ('pdoWithStatus','2', '--buses 7 --nodes 64 --iterations 5')
 
# run_test_multiple_interfaces ('calculatedItems','0')
# run_test_multiple_interfaces ('ngSyncRtr','0')
# run_test_multiple_interfaces ('nmtStartup','0')
# run_test_multiple_interfaces ('sdo','0')

# run_test_multiple_interfaces ('sdoReadWrite',0,'--nodes 64 --iterations 50')
 
# run_test_multiple_interfaces ('segmentedSdo',0, '--buses=1 --nodes=1 --sync_writes --max_len=1000')
 
 
# # run_test_multiple_interfaces ('segmentedSdoMdt',0, '--buses=1 --nodes=2 --sync_interval=10000 --ng_interval=10000 --max_parallel_writes=1 --segmented_sdo_delay="0.03" --dead_elmb_ratio=0')
# # run_test_multiple_interfaces ('segmentedSdoMdt',1, '--buses=7 --nodes=38 --sync_interval=10000 --ng_interval=10000 --max_parallel_writes=3 --segmented_sdo_delay="uniform 0.005 0.1" --dead_elmb_ratio=0.25')
# run_test_multiple_interfaces ('segmentedSdoMdt',2, '--buses=7 --nodes=4 --sync_interval=10000 --ng_interval=10000 --max_parallel_writes=7 --segmented_sdo_delay="uniform 0.005 0.1" --dead_elmb_ratio=0.5')
 
# run_test_multiple_interfaces('canRandomFrames', 0, '--max_testing_time=300')
 
# run_test_multiple_interfaces('crazyThingsOnAddressSpace', 0, '')

# run_test_multiple_interfaces('nonExistingNode', 0, '')

# run_test_multiple_interfaces('variousDatatypes', 0, '')

# running_time=60
# parallel_ops=20
# buses=4
# nodes=4
# def stability1(op):
# 	run_test_multiple_interfaces('stability1', str(op).replace("--",""), op+' --buses='+str(buses)+' --nodes='+str(nodes)+' --max_parallel_ops='+str(parallel_ops)+' --delay=0.001 --running_time='+str(running_time))
# stability1('--do_write_sdo')
# stability1('--do_read_sdo')
# stability1('--do_write_pdo')
# stability1('--do_read_pdo')
# stability1('--do_read_calc_items')
# stability1('--do_read_segmented_sdo')
# stability1('--do_write_segmented_sdo')
# running_time=600
# parallel_ops=32
# buses=4
# nodes=16
# run_test_multiple_interfaces('stability1', 'all', '--do_read_sdo --do_write_sdo --do_read_pdo --do_write_pdo --do_read_segmented_sdo --do_write_segmented_sdo --do_read_calc_items --buses='+str(buses)+' --nodes='+str(nodes)+' --max_parallel_ops='+str(parallel_ops)+' --delay=0.001 --running_time='+str(running_time))

# run_test_multiple_interfaces('stability1', 'all', '--do_read_sdo --do_write_sdo --do_read_pdo --do_write_pdo --do_read_segmented_sdo --do_write_segmented_sdo --do_read_calc_items --buses='+str(buses)+' --nodes='+str(nodes)+' --max_parallel_ops='+str(parallel_ops)+' --delay=0.001 --running_time=60 --logging=1')

# run_test_multiple_interfaces ('stability2',0, '-n 50 -b 3 --do_write_sdo --do_read_sdo --do_write_pdo --do_read_pdo --running_time=300')

# run_test_multiple_interfaces('segmentedSdo','0','--buses=8 --nodes=64 --sync_interval=1000 --ng_interval=1000')
f=open(output_path+'/summary.html','w')
f.write(str(page))
f.close()
